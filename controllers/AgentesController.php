<?php

namespace app\controllers;

use app\models\Agentes;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AgentesController implements the CRUD actions for Agentes model.
 */
class AgentesController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Agentes models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Agentes::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'idAgente' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Agentes model.
     * @param int $idAgente Id Agente
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idAgente)
    {
        return $this->render('view', [
            'model' => $this->findModel($idAgente),
        ]);
    }

    /**
     * Creates a new Agentes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Agentes();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'idAgente' => $model->idAgente]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Agentes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $idAgente Id Agente
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idAgente)
    {
        $model = $this->findModel($idAgente);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idAgente' => $model->idAgente]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Agentes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $idAgente Id Agente
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idAgente)
    {
        $this->findModel($idAgente)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Agentes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $idAgente Id Agente
     * @return Agentes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idAgente)
    {
        if (($model = Agentes::findOne(['idAgente' => $idAgente])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
