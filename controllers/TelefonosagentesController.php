<?php

namespace app\controllers;

use app\models\Telefonosagentes;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TelefonosagentesController implements the CRUD actions for Telefonosagentes model.
 */
class TelefonosagentesController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Telefonosagentes models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Telefonosagentes::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'idTelefonoA' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Telefonosagentes model.
     * @param int $idTelefonoA Id Telefono A
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idTelefonoA)
    {
        return $this->render('view', [
            'model' => $this->findModel($idTelefonoA),
        ]);
    }

    /**
     * Creates a new Telefonosagentes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Telefonosagentes();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'idTelefonoA' => $model->idTelefonoA]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Telefonosagentes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $idTelefonoA Id Telefono A
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idTelefonoA)
    {
        $model = $this->findModel($idTelefonoA);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idTelefonoA' => $model->idTelefonoA]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Telefonosagentes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $idTelefonoA Id Telefono A
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idTelefonoA)
    {
        $this->findModel($idTelefonoA)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Telefonosagentes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $idTelefonoA Id Telefono A
     * @return Telefonosagentes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idTelefonoA)
    {
        if (($model = Telefonosagentes::findOne(['idTelefonoA' => $idTelefonoA])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
