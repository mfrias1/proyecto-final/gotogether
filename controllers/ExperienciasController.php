<?php

namespace app\controllers;

use app\models\Experiencias;
use app\models\ExperienciasSearch;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ExperienciasController implements the CRUD actions for Experiencias model.
 */
class ExperienciasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Experiencias models with details.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new ExperienciasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Experiencias model.
     * @param int $idExperiencia Id Experiencia
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idExperiencia)
    {
        return $this->render('view', [
            'model' => $this->findModel($idExperiencia),
        ]);
    }

    /**
     * Creates a new Experiencias model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Experiencias();

        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        if ($this->request->isPost) {
            $model->idUsuario = Yii::$app->user->id;
            $model->fecha = date('Y-m-d'); // Establecer la fecha actual
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'idExperiencia' => $model->idExperiencia]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Experiencias model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $idExperiencia Id Experiencia
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idExperiencia)
    {
        $model = $this->findModel($idExperiencia);

        // Verificar que el usuario logueado sea el autor del comentario
        if (Yii::$app->user->id != $model->idUsuario) {
            throw new \yii\web\ForbiddenHttpException('No tienes permiso para editar este comentario.');
        }

        if ($this->request->isPost && $model->load($this->request->post())) {
            // Permitir la actualización del comentario y la calificación
            if ($model->save()) {
                return $this->redirect(['view', 'idExperiencia' => $model->idExperiencia]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Experiencias model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $idExperiencia Id Experiencia
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idExperiencia)
    {
        $model = $this->findModel($idExperiencia);

        // Verificar que el usuario logueado sea el autor del comentario
        if (Yii::$app->user->id != $model->idUsuario) {
            throw new \yii\web\ForbiddenHttpException('No tienes permiso para eliminar este comentario.');
        }

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Experiencias model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $idExperiencia Id Experiencia
     * @return Experiencias the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idExperiencia)
    {
        if (($model = Experiencias::findOne(['idExperiencia' => $idExperiencia])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
