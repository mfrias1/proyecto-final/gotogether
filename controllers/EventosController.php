<?php
namespace app\controllers;

use app\models\Eventos;
use app\models\Compras;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Yii;
use yii\web\Response;

/**
 * EventosController implements the CRUD actions for Eventos model.
 */
class EventosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Eventos models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Eventos::find()
                ->where(['>=', 'fecha', date('Y-m-d')]) // Filtrar eventos que no hayan pasado
                ->orderBy(['fecha' => SORT_ASC]), // Ordenar por fecha ascendente
            'pagination' => [
                'pageSize' => 6, // Mostrar solo 6 eventos por página
            ],
        ]);
    
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /**
     * Displays a single Eventos model.
     * @param int $idEvento Id Evento
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idEvento)
    {
        $model = $this->findModel($idEvento);
        $agente = $model->idAgente0; // Obtener el agente relacionado

        // Obtener los teléfonos del agente
        $telefonosAgente = $agente ? $agente->telefonosagentes : [];

        $compraModel = new Compras(); // Crear una instancia del modelo Compras

        return $this->render('view', [
            'model' => $model,
            'agente' => $agente,
            'telefonosAgente' => $telefonosAgente,
            'compraModel' => $compraModel, // Pasar la variable a la vista
        ]);
    }

    /**
     * Creates a new Eventos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Eventos();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'idEvento' => $model->idEvento]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Eventos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $idEvento Id Evento
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idEvento)
    {
        $model = $this->findModel($idEvento);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idEvento' => $model->idEvento]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Eventos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $idEvento Id Evento
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idEvento)
    {
        $this->findModel($idEvento)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Eventos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $idEvento Id Evento
     * @return Eventos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idEvento)
    {
        if (($model = Eventos::findOne(['idEvento' => $idEvento])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /** Buscador */
    public function actionBuscar()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $query = Yii::$app->request->get('query');
        $result = [];

        if ($query) {
            $result = Eventos::find()
                ->where(['like', 'nombrEvento', $query])
                ->all();
        }

        return $this->renderPartial('_resultados', ['resultados' => $result]);
    }
}
