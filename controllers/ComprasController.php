<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use app\models\Eventos;
use app\models\Compras;
use Stripe\Stripe;
use Stripe\Charge;
use Mpdf\Mpdf;

class ComprasController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'add', 'update-cart', 'remove', 'checkout', 'confirm', 'compra-confirmada', 'download-pdf', 'checkout-stripe', 'charge'],
                'rules' => [
                    [
                        'actions' => ['index', 'add', 'update-cart', 'remove', 'checkout', 'confirm', 'compra-confirmada', 'download-pdf', 'checkout-stripe', 'charge'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $cart = Yii::$app->session->get('cart', []);

        // Actualizar precios desde la base de datos
        foreach ($cart as $key => &$item) {
            $event = Eventos::findOne($item['id']);
            if ($event) {
                $item['price'] = $event->precio; // Actualizar el precio
            }
        }
        Yii::$app->session->set('cart', $cart); // Guardar el carrito actualizado

        return $this->render('index', ['cart' => $cart]);
    }

    public function actionAdd()
    {
        $id = Yii::$app->request->post('id');
        $quantity = Yii::$app->request->post('cantidad', 1); // Captura la cantidad, por defecto 1
        if ($id !== null && $quantity !== null) {
            $event = Eventos::findOne($id);
            if ($event) {
                $cart = Yii::$app->session->get('cart', []);
                if (isset($cart[$id])) {
                    $cart[$id]['quantity'] += $quantity; // Suma la cantidad seleccionada
                } else {
                    $cart[$id] = [
                        'id' => $event->idEvento,
                        'name' => $event->nombrEvento,
                        'price' => $event->precio,
                        'quantity' => $quantity, // Establece la cantidad seleccionada
                    ];
                }
                Yii::$app->session->set('cart', $cart);
            }
            return $this->redirect(['compras/index']);
        } else {
            throw new \yii\web\BadRequestHttpException('Parámetros requeridos ausentes: id o cantidad');
        }
    }

    public function actionUpdateCart()
    {
        if (Yii::$app->request->isPost) {
            $id = Yii::$app->request->post('id');
            $quantity = Yii::$app->request->post('quantity');

            if ($id !== null && $quantity > 0) {
                $cart = Yii::$app->session->get('cart', []);
                if (isset($cart[$id])) {
                    $cart[$id]['quantity'] = $quantity;
                    Yii::$app->session->set('cart', $cart);
                }
            }
        }
    }

    public function actionRemove($id)
    {
        $cart = Yii::$app->session->get('cart', []);
        if (isset($cart[$id])) {
            unset($cart[$id]);
            Yii::$app->session->set('cart', $cart);
        }
        return $this->redirect(['compras/index']);
    }

    public function actionCheckout()
    {
        $cart = Yii::$app->session->get('cart', []);
        if (empty($cart)) {
            return $this->redirect(['compras/index']);
        }

        return $this->redirect(['compras/confirm']);
    }

    public function actionConfirm()
    {
        $cart = Yii::$app->session->get('cart', []);
        $purchaseTime = date('Y-m-d H:i:s');
        $userId = Yii::$app->user->id;

        foreach ($cart as $item) {
            $compra = new Compras();
            $compra->idEvento = $item['id'];
            $compra->idUsuario = $userId;
            $compra->cantidad = $item['quantity'];
            $compra->fechaCompra = $purchaseTime;
            $compra->precioCompra = $item['price'];
            $compra->estado = Compras::ESTADO_PAGADO;
            $compra->save();
        }

        Yii::$app->session->remove('cart'); // Limpiar el carrito después de la compra

        return $this->redirect(['compra-confirmada', 'cart' => json_encode($cart), 'purchaseTime' => $purchaseTime]);
    }

    public function actionCompraConfirmada($cart, $purchaseTime)
    {
        $cart = json_decode($cart, true);
        return $this->render('compra-confirmada', [
            'cart' => $cart,
            'purchaseTime' => $purchaseTime
        ]);
    }

    public function actionDownloadPdf($cart, $purchaseTime)
    {
        $cart = json_decode($cart, true);

        // Configurar la fuente personalizada
        $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        $mpdf = new Mpdf([
            'fontDir' => array_merge($fontDirs, [
                Yii::getAlias('@webroot/fonts'),
            ]),
            'fontdata' => $fontData + [
                'permanentmarker' => [
                    'R' => 'PermanentMarker-Regular.ttf',
                ]
            ],
        ]);

        $html = $this->renderPartial('confirm-pdf', ['cart' => $cart, 'purchaseTime' => $purchaseTime]);
        $mpdf->WriteHTML($html);
        $mpdf->Output('comprobante.pdf', 'D'); // Descargar el PDF directamente
    }

    public function actionCancel()
    {
        return $this->render('cancel');
    }

    // Comprar con Stripe
    public function actionCheckoutStripe()
    {
        $cart = Yii::$app->session->get('cart', []);
        if (empty($cart)) {
            return $this->redirect(['compras/index']);
        }

        // Obtener el entorno actual
        $env = Yii::$app->params['stripe.env'];
        $publishableKey = Yii::$app->params['stripe'][$env]['publishableKey'];
        $totalAmount = 0;

        foreach ($cart as $item) {
            $totalAmount += $item['price'] * $item['quantity'];
        }

        return $this->render('checkout-stripe', [
            'cart' => $cart,
            'publishableKey' => $publishableKey,
            'totalAmount' => $totalAmount * 100, // VALOR DE STRIPE EN CENTAVOS
        ]);
    }

    public function actionCharge()
    {
        // Obtener el entorno actual
        $env = Yii::$app->params['stripe.env'];
        \Stripe\Stripe::setApiKey(Yii::$app->params['stripe'][$env]['secretKey']);

        $token = Yii::$app->request->post('stripeToken');
        $totalAmount = Yii::$app->request->post('totalAmount');
        $cart = Yii::$app->session->get('cart', []);
        $purchaseTime = date('Y-m-d H:i:s');
        $userId = Yii::$app->user->id;

        try {
            $charge = \Stripe\Charge::create([
                'amount' => $totalAmount,
                'currency' => 'usd',
                'description' => 'Compra de entradas',
                'source' => $token,
            ]);

            foreach ($cart as $item) {
                $compra = new Compras();
                $compra->idEvento = $item['id'];
                $compra->idUsuario = $userId;
                $compra->cantidad = $item['quantity'];
                $compra->fechaCompra = $purchaseTime;
                $compra->precioCompra = $item['price'];
                $compra->estado = Compras::ESTADO_PAGADO;
                $compra->save();
            }

            Yii::$app->session->remove('cart');

            return $this->redirect(['compra-confirmada', 'cart' => json_encode($cart), 'purchaseTime' => $purchaseTime]);
        } catch (\Exception $e) {
            Yii::$app->session->setFlash('error', 'Error en el pago: ' . $e->getMessage());
            return $this->redirect(['compras/index']);
        }
    }
}
