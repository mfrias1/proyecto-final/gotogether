<?php
namespace app\controllers;

use Yii;
use app\models\Usuarios;
use app\models\UsuariosSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * UsuariosController implements the CRUD actions for Usuarios model.
 */
class UsuariosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
                'access' => [
                    'class' => AccessControl::className(),
                    'only' => ['create', 'update', 'delete', 'profile'],
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Usuarios models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new UsuariosSearch();
        $query = $searchModel->search($this->request->queryParams)->query;

        // Filtrar usuarios con todos los campos completos excepto `idAgente`
        $query->andWhere(['is not', 'nombreU', null])
              ->andWhere(['is not', 'ubicacion', null])
              ->andWhere(['is not', 'intereses', null])
              ->andWhere(['is not', 'imagenU', null])
              ->andWhere(['is not', 'emails', null])
              ->andWhere(['is not', 'password', null]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 8,
            ],
            'sort' => [
                'defaultOrder' => [
                    'idUsuario' => SORT_DESC,
                ],
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Usuarios model.
     * @param int $idUsuario Id Usuario
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idUsuario)
    {
        return $this->render('view', [
            'model' => $this->findModel($idUsuario),
        ]);
    }

    /**
     * Creates a new Usuarios model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Usuarios();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['index']);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Usuarios model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $idUsuario Id Usuario
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idUsuario)
{
    $model = $this->findModel($idUsuario);

    if ($model->load(Yii::$app->request->post()) && $model->save()) {
        if (Yii::$app->request->post('redirect') === 'index') {
            return $this->redirect(['usuarios/index']);
        } else {
            return $this->redirect(['usuarios/index', 'idUsuario' => $model->idUsuario]);
        }
    }

    return $this->render('update', [
        'model' => $model,
    ]);
}


    /**
     * Deletes an existing Usuarios model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $idUsuario Id Usuario
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idUsuario)
    {
        $model = $this->findModel($idUsuario);

        if ($model !== null) {
            $model->delete();
            Yii::$app->user->logout();
            Yii::$app->session->setFlash('success', 'Tu cuenta ha sido eliminada con éxito.');
        }

        return $this->redirect(['site/index']);
    }

    /**
     * Finds the Usuarios model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $idUsuario Id Usuario
     * @return Usuarios the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idUsuario)
    {
        if (($model = Usuarios::findOne($idUsuario)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Displays the profile of the logged-in user.
     *
     * @return string
     */
    public function actionProfile()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $idUsuario = Yii::$app->user->identity->idUsuario;
        $model = $this->findModel($idUsuario);

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()) {
            Yii::$app->session->setFlash('success', 'Perfil actualizado con éxito.');
            return $this->refresh();
        }

        return $this->render('profile', [
            'model' => $model,
        ]);
    }
}
