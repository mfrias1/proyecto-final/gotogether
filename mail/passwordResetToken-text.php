<?php

/* @var $this yii\web\View */
/* @var $user app\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>
Hola <?= $user->emails ?>,

Clica en el siguiente enlace para reestablecer tu contraseña:

<?= $resetLink ?>
