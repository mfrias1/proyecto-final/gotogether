﻿-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-06-2024 a las 12:38:44
-- Versión del servidor: 10.1.40-MariaDB
-- Versión de PHP: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `gotogether`
--

DROP DATABASE IF EXISTS gotogether;
  CREATE DATABASE gotogether;
  USE gotogether
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agentes`
--

CREATE TABLE `agentes` (
  `idAgente` int(11) NOT NULL,
  `nombreA` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `agentes`
--

INSERT INTO `agentes` (`idAgente`, `nombreA`) VALUES
(1, 'Alejandro Pérez'),
(2, 'Isabel Herrera'),
(3, 'Guillermo Sánchez'),
(4, 'Beatriz González'),
(5, 'Martín Vargas'),
(6, 'Adriana Mendoza'),
(7, 'Ricardo Fernández'),
(8, 'Carolina López'),
(9, 'Francisco Ramírez'),
(10, 'Laura Rodríguez'),
(11, 'Sebastián Morales'),
(12, 'Valeria Martínez'),
(13, 'Javier Herrera'),
(14, 'Andrea Gómez'),
(15, 'Ernesto Castro'),
(16, 'Claudia García'),
(17, 'Emilio López'),
(18, 'Valentina Torres'),
(19, 'Mauricio Díaz'),
(20, 'Natalia Soto'),
(21, 'Juan Carlos Ruiz'),
(22, 'Paula Castro'),
(23, 'Alberto Mendoza'),
(24, 'Verónica Ramos'),
(25, 'Daniel Herrera'),
(26, 'Mariana Salazar'),
(27, 'Fernando Ortega'),
(28, 'Camila González'),
(29, 'Sergio Velázquez'),
(30, 'Verónica Pérez');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE `compras` (
  `idCompra` int(11) NOT NULL,
  `idEvento` int(11) DEFAULT NULL,
  `idUsuario` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `fechaCompra` datetime DEFAULT NULL,
  `precioCompra` decimal(10,2) DEFAULT NULL,
  `estado` enum('pendiente','pagado','cancelado') DEFAULT 'pendiente',
  `total` decimal(10,2) AS (`precioCompra` * `cantidad`) VIRTUAL,
  `fechaModificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `compras`
--

INSERT INTO `compras` (`idCompra`, `idEvento`, `idUsuario`, `cantidad`, `fechaCompra`, `precioCompra`, `estado`, `fechaModificacion`) VALUES
(1, 17, NULL, 1, '2024-05-28 14:53:26', '83.23', 'pagado', '2024-05-28 12:53:26'),
(2, 17, 25, 1, '2024-05-29 08:16:22', '83.23', 'pagado', '2024-05-29 06:16:22'),
(3, 17, 25, 1, '2024-05-29 08:35:10', '83.23', 'pagado', '2024-05-29 06:35:10'),
(4, 17, NULL, 2, '2024-05-29 08:37:25', '83.23', 'pagado', '2024-05-29 06:37:25'),
(5, 17, NULL, 2, '2024-05-29 08:41:43', '83.23', 'pagado', '2024-05-29 06:41:43'),
(6, 17, NULL, 1, '2024-05-30 14:11:51', '83.23', 'pagado', '2024-05-30 12:11:51'),
(7, 17, NULL, 5, '2024-05-31 08:17:41', '83.23', 'pagado', '2024-05-31 06:17:41'),
(8, 17, 32, 2, '2024-05-31 08:20:45', '83.23', 'pagado', '2024-05-31 06:20:45'),
(9, 17, 32, 1, '2024-05-31 08:44:26', '83.23', 'pagado', '2024-05-31 06:44:26'),
(10, 17, NULL, 1, '2024-05-31 09:14:02', '83.23', 'pagado', '2024-05-31 07:14:02'),
(11, 17, NULL, 5, '2024-05-31 09:18:22', '83.23', 'pagado', '2024-05-31 07:18:22'),
(12, 17, 32, 1, '2024-05-31 09:26:19', '83.23', 'pagado', '2024-05-31 07:26:19'),
(13, 17, 32, 1, '2024-05-31 09:36:06', '83.23', 'pagado', '2024-05-31 07:36:06'),
(14, 17, 32, 1, '2024-05-31 10:00:44', '83.23', 'pagado', '2024-05-31 08:00:44'),
(15, 25, 32, 1, '2024-05-31 10:00:44', '57.08', 'pagado', '2024-05-31 08:00:44'),
(16, 17, 32, 1, '2024-05-31 10:40:51', '83.23', 'pagado', '2024-05-31 08:40:51'),
(17, 17, 32, 1, '2024-05-31 11:44:51', '83.23', 'pagado', '2024-05-31 09:44:51'),
(18, 14, 32, 1, '2024-05-31 11:44:51', '94.21', 'pagado', '2024-05-31 09:44:51'),
(19, 3, 32, 1, '2024-05-31 12:36:07', '79.62', 'pagado', '2024-05-31 10:36:07'),
(20, 13, 32, 1, '2024-05-31 12:36:07', '108.58', 'pagado', '2024-05-31 10:36:07'),
(21, 17, 32, 1, '2024-05-31 12:40:56', '83.23', 'pagado', '2024-05-31 10:40:56'),
(22, 1, 39, 1, '2024-06-02 17:55:52', '94.70', 'pagado', '2024-06-02 15:55:53'),
(23, 25, 40, 1, '2024-06-03 22:17:17', '57.08', 'pagado', '2024-06-03 20:17:19'),
(24, 25, 40, 1, '2024-06-05 21:08:30', '57.08', 'pagado', '2024-06-05 19:08:33'),
(25, 25, 40, 1, '2024-06-07 00:03:21', '57.08', 'pagado', '2024-06-06 22:03:24');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eventos`
--

CREATE TABLE `eventos` (
  `idEvento` int(11) NOT NULL,
  `nombrEvento` varchar(100) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `lugar` varchar(100) DEFAULT NULL,
  `categoria` varchar(100) DEFAULT NULL,
  `descripcion` varchar(750) DEFAULT NULL,
  `imagenE` varchar(500) DEFAULT NULL,
  `precio` decimal(10,2) DEFAULT NULL,
  `idPatrocinador` int(11) DEFAULT NULL,
  `idAgente` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `eventos`
--

INSERT INTO `eventos` (`idEvento`, `nombrEvento`, `fecha`, `lugar`, `categoria`, `descripcion`, `imagenE`, `precio`, `idPatrocinador`, `idAgente`) VALUES
(1, 'Cuartos de Final FIFA WORLD CUP 2024: Francia vs. Croacia', '2024-06-02', 'Al Bayt, Qatar', 'Partido de fútbol', 'Cuartos de Final FIFA WORLD CUP 2022: Francia vs. Croacia: El partido de cuartos de final de la Copa Mundial de la FIFA 2022 entre Francia y Croacia es un encuentro crucial en el torneo, donde ambos equipos compiten por un lugar en las semifinales. Francia, una de las potencias del fútbol mundial, se enfrenta a Croacia, conocida por su tenacidad y talento. Este partido es una oportunidad para ver a jugadores de clase mundial en acción, ofreciendo un espectáculo lleno de emoción y alta competencia.', 'https://i.ibb.co/SXmRsKY/1200px-2022-FIFA-World-Cup.png', '94.70', 1, 1),
(2, 'Final de FIFA WOMEN\'S WORLD CUP 2023: España vs. Inglaterra', '2023-03-04', 'Brisbane, Australia ', 'Partido de fútbol', 'FIFA WOMEN\'S WORLD CUP 2023: España vs. Inglaterra\r\nEl partido de cuartos de final de la Copa Mundial Femenina de la FIFA 2023 entre España e Inglaterra es un enfrentamiento clave en el torneo, donde ambos equipos luchan por avanzar a las semifinales. España, conocida por su estilo de juego técnico y fluido, se enfrenta a Inglaterra, un equipo con gran fuerza y disciplina táctica. Este partido presenta a algunas de las mejores futbolistas del mundo, ofreciendo un espectáculo lleno de emoción y competencia de alto nivel.', 'https://i.ibb.co/bXWHMpK/1200px-Logo-of-the-2023-FIFA-Women27s-World-Cup.png', '142.75', 2, 2),
(3, 'Gran Premio de Bélgica', '2024-07-11', 'Stavelot, Bélgica', 'Evento de automovilismo', 'Belgica: El Gran Premio de Bélgica de Fórmula 1 es una carrera emblemática en el calendario del campeonato mundial, conocida por su emocionante y desafiante circuito de Spa-Francorchamps. Esta competencia reúne a los mejores pilotos del mundo, compitiendo en un trazado famoso por sus rápidas rectas y complejas curvas.', 'https://i.ibb.co/3yyFs2s/27sfpnl4a4j71.png', '79.62', 3, 3),
(4, 'Lollapalooza', '2021-10-29', 'Buenos Aires, Argentina', 'Festival de música', 'Lollapalooza es un renombrado festival de música que se celebra anualmente, ofreciendo una vibrante mezcla de géneros como rock, pop, hip-hop y electrónica. Fundado en 1991 por Perry Farrell, el evento se ha expandido globalmente, con ediciones en varios países. Atrae a grandes multitudes con actuaciones de artistas de fama mundial y emergentes, creando una experiencia única y diversa.\r\n', 'https://i.ibb.co/zVPzQzx/AIdro-nbk75b0sb1-CY1qsy-YBR1-Gix44z6005-Zt-Ipefno-WVUQh-Tos900-c-k-c0x00ffffff-no-rj.png', '119.84', 4, 4),
(5, 'Cuartos de Final UEFA EURO 2021: Italia vs. Alemania', '2022-09-20', 'Sevilla, España', 'Partido de fútbol', 'El partido de cuartos de final de la UEFA EURO 2021 entre Italia y Alemania es un enfrentamiento destacado en el torneo, con ambos equipos luchando por un puesto en las semifinales. Italia, conocida por su sólida defensa y juego táctico, se enfrenta a Alemania, un equipo con una rica historia y fuerza ofensiva. Este encuentro presenta a algunos de los mejores futbolistas europeos, prometiendo un espectáculo de alta competencia y emoción.\r\n', 'https://i.ibb.co/xStTBtn/logotipo-de-la-uefa-euro-2020-2021-2g16ttd.png', '110.37', 5, 5),
(6, 'Tomorrowland', '2020-02-19', 'Boom, Bélgica', 'Festival de música', 'Tomorrowland es uno de los festivales de música electrónica más grandes y populares del mundo, celebrado anualmente en Boom, Bélgica. Fundado en 2005, el evento reúne a los mejores DJ y productores del género, ofreciendo actuaciones espectaculares en escenarios innovadores. Con su ambiente mágico y producción de alta calidad, Tomorrowland atrae a cientos de miles de asistentes de diversas nacionalidades\r\n', 'https://i.ibb.co/x31zKT3/raf360x360075tfafafaca443f4786.png', '142.30', 6, 6),
(7, 'UEFA Champions League: Liverpool vs. Tottenham Hotspur', '2020-08-10', 'Liverpool, Inglaterra', 'Partido de fútbol', 'El enfrentamiento entre Liverpool y Tottenham Hotspur en la UEFA Champions League es un duelo de alto nivel en el fútbol europeo, especialmente destacado por su final en la temporada 2018-2019. Liverpool, conocido por su intenso estilo de juego y rica historia en competiciones europeas, se mide ante Tottenham, un equipo con una creciente reputación y talento emergente.\r\n', 'https://i.ibb.co/5kBVJt6/Design-Poster3-5000x.png', '130.42', 7, 7),
(8, 'El Clásico - FC Barcelona vs. Real Madrid', '2020-01-02', 'Barcelona, España', 'Partido de fútbol', 'El Clásico es el enfrentamiento entre FC Barcelona y Real Madrid, dos de los clubes más prestigiosos y exitosos del fútbol mundial. Este partido es uno de los eventos deportivos más seguidos a nivel global, conocido por su intensa rivalidad histórica y cultural. FC Barcelona, con su estilo de juego basado en la posesión y creatividad, se enfrenta a Real Madrid, famoso por su poderío ofensivo y estrellas internacionales. Cada encuentro ofrece un espectáculo de talento y emoción, con implicaciones significativas en la lucha por títulos nacionales e internacionales\r\n', 'https://i.ibb.co/SNLDRL4/16789724584655.png', '75.19', 8, 8),
(9, 'Gran Premio de Singapur', '2026-11-11', 'Marina Bay, Singapur', 'Evento de automovilismo', 'El Gran Premio de Singapur es una destacada carrera de Fórmula 1, famosa por ser la primera carrera nocturna en la historia del campeonato. Celebrada en el Marina Bay Street Circuit, el evento ofrece un emocionante desafío con su trazado urbano y condiciones de carrera bajo las luces artificiales. Esta carrera es conocida por su espectacular ambiente, con el circuito iluminado que crea una atmósfera única y vibrante.\r\n', 'https://i.ibb.co/nCphnmH/alpine-f1-team-poster-for-the-singapore-grand-prix-v0-1dqdpa6njsnb1.png', '134.69', 9, 9),
(10, 'Electric Daisy Carnival', '2024-07-07', 'Nevada, Estados Unidos', 'Festival de música', 'El Electric Daisy Carnival (EDC) es un festival de música electrónica de baile organizado por Insomniac Events. Celebrado anualmente en el Las Vegas Motor Speedway, es uno de los festivales más grandes y populares del género en Norteamérica. EDC ofrece una experiencia multisensorial única que combina música de renombrados DJs y productores, arte innovador, instalaciones tecnológicas y espectáculos pirotécnicos.\r\n', 'https://i.ibb.co/MCxkFsB/9a7fd39d43c6ff3b36341c7e41fad11a.png', '97.87', 10, 10),
(11, 'Amistoso Internacional de FIFA: Alemania vs. Argentina', '2020-11-07', 'Johhanesburgo, Sudáfrica', 'Partido de fútbol', 'El Amistoso Internacional de FIFA entre Alemania y Argentina es un evento futbolístico significativo que reúne a dos de las selecciones más prestigiosas del mundo. Estos partidos, organizados en diversas fechas del calendario de la FIFA, sirven como preparación para competiciones oficiales y ofrecen a los entrenadores la oportunidad de probar nuevas alineaciones y estrategias, además de brindar experiencia a jugadores jóvenes y emergentes.\r\n', 'https://i.ibb.co/jbfZXTB/15706282485066.jpg', '135.30', 11, 11),
(12, 'Sziget Festival', '2020-01-29', 'Budapest, Hungría', 'Festival de música', 'El Sziget Festival es uno de los eventos de música y cultura más grandes y reconocidos de Europa, celebrado anualmente en la isla Óbuda en Budapest, Hungría. Este festival, conocido como \"La Isla de la Libertad\", ofrece una experiencia diversa e inclusiva que atrae a más de 500,000 asistentes de todo el mundo. El Sziget Festival destaca por su enfoque en la diversidad artística, presentando más de 60 escenarios con una amplia variedad de géneros musicales, desde rock y pop hasta música electrónica, jazz, y world music. ', 'https://i.ibb.co/5rYMsfM/sziget-festival-2023.jpg', '132.90', 12, 12),
(13, 'Roskilde Festival', '2024-06-25', 'Roskilde, Dinamarca', 'Festival de música', 'El Roskilde Festival es uno de los festivales de música más grandes y antiguos de Europa, celebrado anualmente en Roskilde, Dinamarca. Fundado en 1971 por dos estudiantes de secundaria, el festival ha crecido significativamente, atrayendo a más de 130,000 asistentes cada año, junto con 30,000 voluntarios y miles de artistas y medios de comunicación. Este evento es conocido por su enfoque en la música contemporánea, abarcando géneros como rock, metal, música electrónica, hip-hop y ritmos globales.\r\n', 'https://i.ibb.co/wCmW2Tp/roskildefest.jpg', '108.58', 13, 13),
(14, 'Octavos de final de la UEFA Champions League: FC Barcelona vs. Paris Saint-Germain (PSG)', '2024-05-23', 'Barcelona, España', 'Partido de fútbol', 'El partido de octavos de final de la UEFA Champions League entre el FC Barcelona y el Paris Saint-Germain (PSG) es uno de los enfrentamientos más esperados del torneo. Este duelo reúne a dos de los clubes más poderosos y con mayor historia en el fútbol europeo, prometiendo un espectáculo lleno de talento y emoción.\r\n', 'https://i.ibb.co/p4qC6rx/FYJQJHKNPVDFBNLWFK42-XCNBEU.png', '94.21', 14, 14),
(15, 'Glastonbury Festival', '2025-07-27', 'Somerset, Inglaterra', 'Festival de música', 'El Glastonbury Festival es uno de los festivales de música y artes escénicas más grandes y reconocidos del mundo. Celebrado en Worthy Farm, Pilton, Somerset, Inglaterra, este evento de cinco días reúne a una gran diversidad de géneros musicales, incluyendo rock, pop, hip-hop, música electrónica y folk. Además de los espectáculos musicales, el festival cuenta con áreas dedicadas a la danza, comedia, teatro, circo, y cabaret, ofreciendo una experiencia cultural completa.\r\n', 'https://i.ibb.co/9q8s5Cf/3217-1678291678-fest.png', '95.29', 15, 15),
(16, 'Copa Libertadores: River Plate vs. Boca Juniors', '2021-07-21', 'Buenos Aires, Argentina', 'Partido de fútbol', 'El enfrentamiento de la Copa Libertadores entre River Plate y Boca Juniors es conocido como el \"Superclásico argentino\", y es uno de los partidos más intensos y apasionantes del fútbol mundial. Ambos equipos de Buenos Aires tienen una histórica rivalidad que se intensifica en la competición continental. Este partido es un verdadero espectáculo de emociones, con jugadores clave que pueden definir el resultado y estrategias tácticas cruciales.\r\n', 'https://ibb.co/8zLFFHK', '143.82', 16, 16),
(17, 'Ultra Music Festival', '2024-07-30', 'Miami, Estados Unidos', 'Festival de música', 'El Ultra Music Festival es uno de los festivales de música electrónica más destacados y populares del mundo, celebrado anualmente en marzo en Bayfront Park, Miami, Florida. Fundado en 1999 por Russell Faibisch y Alex Omes, el festival ha crecido enormemente desde sus inicios como un evento de un día hasta convertirse en un evento de tres días. Actualmente, atrae a cientos de miles de fanáticos de la música electrónica de todo el mundo, ofreciendo una experiencia inigualable con actuaciones de los mejores DJs y artistas del género.\r\n\r\n\r\n\r\n\r\n\r\n\r\n', 'https://i.ibb.co/r5fmZnM/ultramizedd2023-0324-181412-09324-ime-edit.png', '1.00', 17, 17),
(18, 'Coachella', '2026-01-20', 'California, Estados Unidos ', 'Festival de música', 'El Festival de Música y Artes de Coachella Valley, conocido simplemente como Coachella, es uno de los eventos de música y arte más importantes y populares del mundo. Se lleva a cabo anualmente en Indio, California, generalmente durante dos fines de semana consecutivos en abril. Coachella comenzó en 1999, creado por Paul Tollett y Rick Van Santen, y desde entonces ha crecido enormemente en popularidad y tamaño. En sus primeros años, el festival se centraba principalmente en géneros alternativos, rock y electrónica, pero con el tiempo ha ampliado su alcance para incluir una amplia variedad de géneros musicales, desde hip hop hasta pop y música latina.ç\r\n', 'https://i.ibb.co/0K71pbh/1df8950a453c2ef950e4b0dd7fd47e4c.png', '134.68', 18, 18),
(19, 'Gran Premio de Japón', '2024-11-30', 'Suzuka, Japón', 'Evento de automovilismo', 'El Gran Premio de Japón de Fórmula 1, celebrado en el Circuito de Suzuka, es una de las carreras más emblemáticas y esperadas. Desde 1987, este evento ha sido fundamental en el calendario, atrayendo tanto a pilotos como a aficionados. Suzuka, ubicado en la prefectura de Mie, es conocido por su diseño en forma de ocho, con curvas rápidas, chicanes y cambios de elevación. Curvas icónicas como la 130R y la Degner Curve ponen a prueba la habilidad de los pilotos. La atmósfera es electrizante, con una apasionada base de seguidores japoneses que llenan las gradas con colores y banderas de sus equipos y pilotos favoritos.\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n', 'https://i.ibb.co/yYJD6VM/flat750x075f-pad750x1000f8f8f8.png', '73.70', 19, 19),
(20, 'Final de la UEFA EURO 2024', '2024-07-14', 'Wembley, Inglaterra', 'Partido de fútbol', 'La UEFA EURO 2020, celebrada en 2021 debido a la pandemia de COVID-19, es la 16ª edición del Campeonato Europeo de Fútbol de la UEFA. Este torneo se caracteriza por su formato innovador, llevándose a cabo en 11 ciudades de diferentes países europeos en lugar de en una única nación anfitriona. Celebrada del 11 de junio al 11 de julio de 2021, la competición destaca por su alta calidad futbolística y la diversidad cultural de los equipos participantes. La final tiene lugar en el icónico estadio de Wembley en Londres. A pesar de las restricciones impuestas por la pandemia, el torneo cuenta con la pasión de los aficionados y una atmósfera vibrante, consolidándose como un símbolo de resiliencia y unidad en tiempos de desafío global.\r\n', 'https://i.ibb.co/WNChk8v/eurocopa21.jpg', '114.48', 20, 20),
(21, 'Final de la FIFA WORLD CUP 2022: Francia vs. Argentina', '2021-09-07', 'Lusail, Qatar', 'Partido de fútbol', 'La final del Mundial 2022 se celebra en el estadio Lusail en Lusail, Qatar. Este evento marca el clímax del torneo más prestigioso del fútbol internacional, organizado por la FIFA. La competición destaca por reunir a las mejores selecciones nacionales de todo el mundo, ofreciendo partidos de alta calidad y momentos memorables. La final, que tiene lugar en un estadio de última generación, es seguida por millones de aficionados en todo el planeta, tanto en directo como a través de transmisiones televisivas.\r\n', 'https://i.ibb.co/GHtBzK3/1ea8f8918c40b3bf952a3bed238a6910.png', '101.31', 1, 21),
(22, 'Reading and Leeds Festivals', '2027-07-03', 'Leeds, Inglaterra', 'Festival de música', 'El Reading and Leeds Festivals son dos de los eventos musicales más emblemáticos del Reino Unido, celebrándose simultáneamente durante el fin de semana del Día del Banco en agosto. Ambos festivales comparten el mismo cartel, con los artistas rotando entre las dos ciudades. El Reading Festival tiene lugar en Richfield Avenue en Reading, mientras que el Leeds Festival se celebra en Bramham Park en Leeds.\r\n\r\nEstos festivales son conocidos por su ecléctica mezcla de géneros musicales, abarcando rock, indie, punk y hip-hop, atrayendo a una multitud diversa de aficionados. Los escenarios principales presentan a algunos de los nombres más grandes y reconocidos de la industria musical.', 'https://i.ibb.co/VSj8hPT/RL24-Lineup-Rockstar-Square-8-April-24.png', '113.09', 2, 22),
(23, 'Gran Premio de Bahrein', '2025-02-04', 'Sakhir, Bahrein', 'Evento de automovilismo', 'El Gran Premio de Baréin de Fórmula 1, celebrado en el Circuito Internacional de Baréin en Sakhir, es una de las carreras más destacadas del calendario. Este evento anual, a menudo inaugurando el campeonato, se realiza en una pista de 5.412 kilómetros, inaugurada en 2004, famosa por su diseño moderno y sus instalaciones de alta calidad. La combinación de rectas largas y curvas desafiantes convierte la pista en un reto para pilotos e ingenieros. Desde 2014, la carrera se celebra al atardecer y termina bajo las luces, creando un espectacular escenario nocturno que añade un atractivo especial al evento.\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n', 'https://i.ibb.co/0Cn6MNp/Fq-JK0y1-WYAI-e8u.png', '111.52', 3, 23),
(24, 'Primavera Sound', '2025-04-01', 'Barcelona, España', 'Festival de música', 'El Primavera Sound es un festival de música que se celebra anualmente en Barcelona, España, y ocasionalmente en otras ciudades como Oporto, Portugal. Es reconocido por su diversidad musical y su enfoque en artistas emergentes y establecidos de diversos géneros, incluyendo indie, rock, electrónica, pop, hip-hop y más.\r\n\r\nCon una duración de varios días, el Primavera Sound ofrece una experiencia inmersiva para los asistentes, con múltiples escenarios que presentan actuaciones simultáneas de artistas de renombre mundial, así como de talentos locales y regionales. El festival también destaca por su compromiso con la igualdad de género en la programación de artistas, promoviendo la presencia de mujeres en la industria musical.', 'https://i.ibb.co/6XJPfyt/3537d3621d24555dffe27c9a1819bd5d.png', '68.33', 4, 24),
(25, 'Gran Premio de Mónaco', '2024-06-20', 'Montecarlo, Mónaco', 'Evento de automovilismo', 'Carrera de Fórmula 1 que Se lleva a cabo en las estrechas y sinuosas calles del Principado de Mónaco, ofreciendo un desafío único para los pilotos y una experiencia emocionante para los espectadores.\r\n\r\nLa carrera se destaca por su glamour y su ambiente exclusivo, con la jet set internacional acudiendo en masa para presenciar el evento desde yates lujosos, balcones de hoteles y terrazas VIP. La historia del Gran Premio de Mónaco está impregnada de leyendas y momentos memorables, con nombres como Ayrton Senna, Graham Hill y Michael Schumacher dejando su huella en las calles de Montecarlo.', 'https://i.ibb.co/DLBzL1g/il-570xN.png', '57.08', 5, 25),
(26, 'Gran Premio de China', '2027-12-11', 'Shangai, China', 'Evento de automovilismo', 'El Gran Premio de China es una emocionante carrera de la Fórmula 1 que se lleva a cabo en el Circuito Internacional de Shanghái en Shanghái, China. Esta carrera es una parte importante del calendario de la Fórmula 1, atrayendo a equipos, pilotos y fanáticos de todo el mundo.\r\n\r\nEl Circuito Internacional de Shanghái ofrece un desafío único para los pilotos, con una combinación de curvas rápidas y lentas, así como una de las rectas más largas del calendario de la Fórmula 1. Esta variedad de secciones hace que la carrera sea emocionante y estratégica, con oportunidades para adelantar y maniobrar en cada vuelta.', 'https://i.ibb.co/CWQ15Bp/113946.png', '130.43', 6, 26),
(27, 'Final UEFA Champions League: Real Madrid vs. Borussia Dortmund', '2024-06-01', 'Londres, Inglaterra', 'Partido de fútbol', 'La UEFA Champions League es el torneo de clubes de fútbol más prestigioso de Europa y uno de los eventos deportivos más emocionantes del mundo. Reúne a los mejores equipos de las ligas europeas en una competición que captura la atención de millones de fanáticos en todo el mundo.\r\n\r\nCada temporada, los equipos compiten en una serie de etapas, desde la fase de grupos hasta la emocionante fase eliminatoria, en la que los equipos luchan por avanzar hacia la final y alcanzar la gloria europea. La UEFA Champions League no solo ofrece partidos de alta calidad y emocionantes enfrentamientos entre clubes de renombre, sino que también es una plataforma para que los jugadores demuestren su habilidad y talento en el escenario más grande.', 'https://i.ibb.co/ySfJfhB/fbed541228783e86035e4e7dddf1adbf.png', '130.91', 7, 27),
(28, 'Seminifinal de FIFA WORLD CUP 2022: Inglaterra vs. Francia', '2023-11-16', 'Doha, Qatar', 'Partido de fútbol', 'Seminifinal de FIFA WORLD CUP 2022: Inglaterra vs. Francia\r\nEl enfrentamiento en semifinales entre Inglaterra y Francia en la Copa Mundial de la FIFA 2022 promete ser un choque épico entre dos potencias del fútbol mundial. Ambos equipos cuentan con una rica historia futbolística y una gran cantidad de talento en sus filas, lo que asegura un partido lleno de emoción y drama.\r\n\r\nInglaterra, con su legado en el fútbol y su ferviente base de fanáticos, busca hacer historia una vez más en el escenario mundial. Con jugadores talentosos y un enfoque táctico disciplinado, el equipo inglés aspira a alcanzar la final y reclamar la gloria para su país.', 'https://i.ibb.co/YhZ4Ggk/2-XFQ6-CXL7-NHYZMVHJSR6-K3-D3-NQ.jpg', '113.26', 8, 28),
(29, 'Gran Premio de Australia', '2028-05-21', 'Melbourne, Australia', 'Evento de automovilismo', 'El Gran Premio de Australia es la carrera de apertura del campeonato de Fórmula 1 y marca el inicio de la temporada de carreras de motor con un estallido de emoción y anticipación. Se celebra en el Circuito de Albert Park en Melbourne, un circuito urbano que ofrece un emocionante desafío para los pilotos y una experiencia inigualable para los espectadores.\r\n\r\nLa carrera en Australia es más que una simple competencia; es un evento que celebra la pasión por el automovilismo y atrae a aficionados de todo el mundo. La atmósfera en Melbourne durante el Gran Premio es electrificante, con fanáticos ansiosos por ver a sus equipos y pilotos favoritos en acción después de meses de espera.', 'https://i.ibb.co/vxPNLkp/04d696d71dc87851f6df1e1b77414c87.png', '123.56', 9, 29),
(30, 'Gran Premio de Estados Unidos', '2023-01-13', 'Austin, texas', 'Evento de automovilismo', 'El Gran Premio de Fórmula 1 en Austin, Texas, es uno de los eventos más esperados en el calendario de carreras de motor. Se lleva a cabo en el Circuito de las Américas, un circuito de última generación que ofrece emocionantes curvas y rectas largas, desafiando a los pilotos y emocionando a los aficionados.\r\n\r\nEl Gran Premio de Austin no solo es una oportunidad para presenciar la acción trepidante de la Fórmula 1, sino que también es una celebración de la cultura y la hospitalidad texana. Los aficionados pueden disfrutar de una variedad de actividades antes y después de la carrera, incluyendo conciertos de música en vivo, eventos gastronómicos con comida local y experiencias únicas de entretenimiento.\r\n', 'https://i.ibb.co/GHMyvJx/F8u-Z-fx-XIAEbw-Al.png', '128.02', 10, 30);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `experiencias`
--

CREATE TABLE `experiencias` (
  `idExperiencia` int(11) NOT NULL,
  `idEvento` int(11) DEFAULT NULL,
  `idUsuario` int(11) DEFAULT NULL,
  `calificacion` int(11) DEFAULT NULL,
  `comentario` varchar(250) DEFAULT NULL,
  `fecha` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `experiencias`
--

INSERT INTO `experiencias` (`idExperiencia`, `idEvento`, `idUsuario`, `calificacion`, `comentario`, `fecha`) VALUES
(1, 1, 1, 2, '\"¡Un emocionante enfrentamiento entre dos potencias futbolísticas! Francia vs. Croacia promete un duelo de titanes en la cancha.\"', '2022-01-03'),
(2, 2, 2, 3, '\"Dos equipos femeninos destacados compiten por el título mundial. España vs. Inglaterra: ¡un enfrentamiento lleno de talento y pasión!\"', '2022-12-19'),
(3, 3, 3, 5, '\"El Gran Premio de Bélgica ofrece velocidad, emoción y adrenalina pura. Los aficionados disfrutan de la emoción de la Fórmula 1 en uno de los circuitos más desafiantes.\"', '2023-04-13'),
(4, 4, 4, 1, '\"¡Lollapalooza es una experiencia musical inigualable! Un festival donde la música, la diversión y la energía se fusionan para crear recuerdos inolvidables.\"', '2022-04-07'),
(5, 5, 5, 3, '\"Italia vs. Alemania en los Cuartos de Final de la UEFA EURO 2021: dos equipos legendarios compiten por un lugar en las semifinales. ¡Acción garantizada!\"', '2020-03-21'),
(6, 6, 6, 2, '\"Tomorrowland es un sueño hecho realidad para los amantes de la música electrónica. Un festival mágico que transporta a sus asistentes a otro mundo lleno de ritmo y alegría.\"', '2021-07-29'),
(7, 7, 7, 5, '\"Liverpool vs. Tottenham Hotspur en la UEFA Champions League: una batalla épica entre dos gigantes del fútbol europeo. ¡Prepárense para un espectáculo inolvidable!\"', '2021-09-16'),
(8, 8, 8, 4, '\"El Clásico entre el FC Barcelona y el Real Madrid es más que un partido de fútbol, es una rivalidad legendaria que trasciende el deporte. ¡Emoción en estado puro!\"', '2020-07-16'),
(9, 9, 9, 1, '\"El Gran Premio de Singapur ofrece una carrera nocturna única en un circuito urbano impresionante. ¡Un evento imperdible para los fanáticos de la Fórmula 1!\"', '2023-10-16'),
(10, 10, 10, 1, '\"Electric Daisy Carnival es el paraíso de la música electrónica. Un festival lleno de luz, color y sonido que crea una atmósfera de pura euforia.\"', '2024-02-09'),
(11, 11, 11, 3, '\"Un enfrentamiento clásico que nunca decepciona. Alemania vs. Argentina: ¡expectativas altas y fútbol de calidad asegurado!\"', '2023-02-09'),
(12, 12, 12, 1, '\"Sziget es mucho más que un festival, es una experiencia de vida. Música, arte y diversión en un ambiente único. ¡Imprescindible!\"', '2021-11-03'),
(13, 13, 13, 1, '\"Roskilde Festival es la perfecta combinación de música y comunidad. Una experiencia enriquecedora que deja recuerdos para toda la vida.\"', '2021-05-13'),
(14, 14, 14, 2, '\"Barcelona vs. PSG: un choque de titanes en la Champions League. Emoción, habilidad y pasión se unen en este épico enfrentamiento.\"', '2022-06-14'),
(15, 15, 15, 4, '\"Glastonbury es el sueño de cualquier amante de la música. Una fusión de géneros y culturas que crea una atmósfera mágica e inolvidable.\"', '2021-09-02'),
(16, 16, 16, 2, '\"El Superclásico del fútbol sudamericano: River Plate vs. Boca Juniors. Una rivalidad legendaria que siempre ofrece un espectáculo emocionante.\"', '2021-03-07'),
(17, 17, 17, 2, '\"Ultra es el paraíso de la música electrónica. Un festival lleno de energía y momentos épicos que quedan grabados en la memoria para siempre.\"', '2020-08-13'),
(18, 18, 18, 5, '\"Coachella es más que un festival, es un estilo de vida. Música, moda y arte se fusionan en un ambiente único y vibrante. ¡Simplemente increíble!\"', '2020-05-15'),
(19, 19, 19, 1, '\"El Gran Premio de Japón es una experiencia única para los aficionados a la Fórmula 1. Velocidad, tecnología y emoción en el legendario circuito de Suzuka.\"', '2022-03-12'),
(20, 20, 20, 4, '\"Italia vs. Inglaterra en la final de la Euro: un choque de estilos y una batalla por la gloria. ¡Un partido que dejará huella en la historia del fútbol!\"', '2022-09-13'),
(21, 21, 1, 4, '\"La final soñada: Francia vs. Argentina. Dos potencias del fútbol luchando por la gloria mundial. ¡Un partido épico que no te puedes perder!\"', '2023-06-05'),
(22, 22, 2, 1, '\"Reading y Leeds: el paraíso de los amantes de la música. Una experiencia inigualable de diversión, camaradería y grandes actuaciones en vivo.\"', '2020-01-09'),
(23, 23, 3, 1, '\"El Gran Premio de Bahrein ofrece emoción y velocidad en un escenario impresionante. Un evento imperdible para los fanáticos de la Fórmula 1.\"', '2023-01-21'),
(24, 24, 4, 4, '\"Primavera Sound es el lugar donde convergen la música y la cultura. Un festival ecléctico que celebra la diversidad y la creatividad artística.\"', '2023-06-23'),
(25, 25, 5, 3, '\"Mónaco: el Gran Premio más prestigioso de la Fórmula 1. Velocidad, glamour y emoción se combinan en las calles de Monte Carlo. ¡Una experiencia única!\"', '2021-08-06'),
(26, 26, 6, 4, '\"El Gran Premio de China ofrece acción y adrenalina en el circuito de Shanghai. Un evento emocionante que cautiva a los aficionados de la Fórmula 1.\"', '2020-03-09'),
(27, 27, 7, 3, '\"AC Milan vs. Liverpool en la Champions League: un choque de gigantes europeos. Emoción, talento y pasión en cada minuto. ¡Un partido para recordar!\"', '2024-03-01'),
(28, 28, 8, 1, '\"Inglaterra vs. Francia en las semifinales: un duelo titánico por un lugar en la final. ¡Dos equipos en su mejor momento dispuestos a todo por la victoria!\"', '2021-04-27'),
(29, 29, 9, 3, '\"El Gran Premio de Australia es el inicio emocionante de la temporada de Fórmula 1. Velocidad, drama y competencia en el circuito de Melbourne.\"', '2020-08-28'),
(30, 30, 10, 5, '\"El Gran Premio de Estados Unidos es una fiesta de velocidad en el Circuito de las Américas. Un evento emocionante para los fans de la Fórmula 1.\"', '2021-01-19'),
(31, 4, 25, 3, 'Me ha encantando el evento el muy divertido', '2024-05-29'),
(32, 2, 25, 5, 'pureba', '2024-05-29'),
(33, 7, 25, 3, 'prueba botones', '2024-05-30'),
(34, 1, 25, NULL, 'srths', '2024-05-29'),
(35, 2, 25, 3, 'prueba', '2022-01-02'),
(36, 6, 25, 1, 'me estoy muriendo', '2024-05-29'),
(37, 3, 25, 5, 'prueba estrellas', NULL),
(38, 3, 25, 5, 'prueba estrellla', NULL),
(39, 6, 25, NULL, 'prueba fecha', '2024-05-29'),
(40, 8, 25, NULL, 'Prueba final', '2024-05-29'),
(41, 5, 29, 5, 'fgdgs', '2024-05-30'),
(42, 1, 32, 3, 'Prueba experiencia', '2024-05-31'),
(43, 18, 39, 5, 'qfwef', '2024-06-02'),
(44, 11, 39, 5, 'dafdfasf', '2024-06-02'),
(45, 5, 40, 5, 'gwgwg', '2024-06-03'),
(46, NULL, 40, NULL, '', '2024-06-03'),
(47, 19, 40, 5, 'prueba fecha', '2024-06-08'),
(48, 9, 40, 2, 'botones', '2024-06-08'),
(49, 6, 40, 5, 'etje6t', '2024-06-08'),
(50, 10, 40, 5, 'eewqg', '2024-06-08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `telefonosagentes`
--

CREATE TABLE `telefonosagentes` (
  `idTelefonoA` int(11) NOT NULL,
  `idAgente` int(11) DEFAULT NULL,
  `telefonoAgente` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `telefonosagentes`
--

INSERT INTO `telefonosagentes` (`idTelefonoA`, `idAgente`, `telefonoAgente`) VALUES
(13, 3, '+34 680 456 789'),
(2, 4, '+34 660 234 567'),
(19, 4, '+34 697 234 567'),
(24, 5, '+34 691 678 901'),
(11, 5, '+54 9 11 2345 6789 '),
(30, 5, '+55 11 94567 8901 '),
(14, 6, '+34 610 567 890'),
(17, 7, '+34 620 345 678'),
(1, 7, '+34 680 456 789'),
(27, 8, '+34 614 901 234'),
(8, 8, '+34 697 234 567'),
(16, 9, '+55 11 94567 8901 '),
(23, 10, '+34 694 901 234'),
(6, 13, '+34 694 901 234'),
(10, 14, '+34 691 678 901'),
(33, 15, '+57 311 234 5678 '),
(32, 16, '+34 630 456 789'),
(22, 16, '+34 670 345 678'),
(28, 17, '+57 311 234 5678 '),
(34, 18, '+57 311 234 5678 '),
(18, 19, '+34 620 345 678'),
(29, 20, '+34 614 901 234'),
(4, 21, '+34 650 123 456'),
(3, 21, '+56 9 1234 5678 '),
(12, 22, '+34 630 456 789'),
(15, 23, '+51 934 567 890 '),
(26, 24, '+34 695 012 345'),
(31, 25, '+34 670 345 678'),
(7, 26, '+34 691 678 901'),
(9, 28, '+34 670 345 678'),
(5, 28, '+34 694 901 234'),
(21, 29, '+34 698 345 678'),
(20, 29, '+57 311 234 5678 '),
(25, 30, '+34 615 012 345');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idUsuario` int(11) NOT NULL,
  `idAgente` int(11) DEFAULT NULL,
  `nombreU` varchar(250) DEFAULT NULL,
  `ubicacion` varchar(100) DEFAULT NULL,
  `intereses` varchar(250) DEFAULT NULL,
  `imagenU` varchar(500) DEFAULT NULL,
  `emails` varchar(150) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `rol` varchar(50) DEFAULT 'usuario',
  `password_reset_token` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idUsuario`, `idAgente`, `nombreU`, `ubicacion`, `intereses`, `imagenU`, `emails`, `password`, `rol`, `password_reset_token`, `status`) VALUES
(1, NULL, 'jabi', NULL, NULL, 'https://i.ibb.co/fGLknC8/profile4.png', 'jabi@mail.com', '1234', NULL, NULL, 1),
(2, NULL, 'manu', NULL, NULL, 'https://i.ibb.co/gdkKt55/OIG1.png', 'manu@mail.com', '1234', NULL, NULL, 1),
(3, 1, 'O.Ortiz.Mulero', 'Madrid, España', 'Ciclismo', 'https://i.ibb.co/WFdd1y9/792e4dfa-9f42-4623-89c1-7a1950a71ca5.webp', 'ortiz.mulero@accidentebicicleta.com', '1234', 'usuario', NULL, 1),
(4, 2, 'MarLop', 'Murcia, España', 'Festivales de música electrónica', 'https://i.ibb.co/gdkKt55/OIG1.png', 'maria.lopez@email.com', '1234', 'usuario', NULL, 1),
(5, 3, 'CGarcia', 'Badajoz, España', 'Torneos de deportes extremos', 'https://i.ibb.co/k9Ck1Bs/OIG2.png', 'carlos.garcia@email.org', '1234', 'usuario', NULL, 1),
(6, 4, 'LRodri', 'Lima, Perú', 'Exhibiciones de arte contemporáneo', 'https://i.ibb.co/fGLknC8/profile4.png', 'luis.rodriguez@mail.net', '1234', 'usuario', NULL, 1),
(7, 5, 'APereznator', 'Málaga, España', 'Competiciones de eSports', 'https://i.ibb.co/CBH2Nvx/OIG3.png', 'alejandro.perez@email.net', '1234', 'usuario', NULL, 1),
(8, 6, 'AnaGon', 'Salamanca, España', 'Desfiles de moda ', 'https://i.ibb.co/k9Ck1Bs/OIG2.png', 'ana.gonzalez.mail.net', '1234', 'usuario', NULL, 1),
(9, 7, 'JSanx', 'Bogotá, Colombia', 'Competiciones de eSports', 'https://i.ibb.co/gdkKt55/OIG1.png', 'jorge.sanchez.email.org', '1234', 'usuario', NULL, 1),
(10, 8, 'IRami', 'Valencia, España', 'Desfiles de moda ', 'https://i.ibb.co/CBH2Nvx/OIG3.png', 'andres.molina@email.com', '1234', 'usuario', NULL, 1),
(11, 9, 'LuTo7', 'Montevideo, Uruguay', 'Partidos de fútbol ', 'https://i.ibb.co/k9Ck1Bs/OIG2.png', 'luis.torres@example.com', '1234', 'usuario', NULL, 1),
(12, 10, 'PDiazz', 'Asturias, España', 'Partidos de fútbol ', 'https://i.ibb.co/CBH2Nvx/OIG3.png', 'paula.diaz@mail.com', '1234', 'usuario', NULL, 1),
(13, 11, 'SVargas', 'Las Palmas de Gran Canaria, España', 'Competiciones de baile', 'https://i.ibb.co/gdkKt55/OIG1.png', 'santiago.vargas@mail.com', '1234', 'usuario', NULL, 1),
(14, 12, 'CHerr', 'Zaragoza, España', 'Torneos de ajedrez ', 'https://i.ibb.co/CBH2Nvx/OIG3.png', 'carmen.herrera@mail.org', '1234', 'usuario', NULL, 1),
(15, 13, 'MJimenator', 'Quito, Ecuador', 'Conferencias de tecnología', 'https://i.ibb.co/k9Ck1Bs/OIG2.png', 'miguel.mendoza@mail.org', '1234', 'usuario', NULL, 1),
(16, 14, 'RFlo', 'Buenos Aires, Argentina', 'Partidos de fútbol ', 'https://i.ibb.co/fGLknC8/profile4.png', 'alberto.valdez@email.net', '1234', 'usuario', NULL, 1),
(17, 15, 'FranNu', 'Lima, Perú', 'Partidos de fútbol ', 'https://i.ibb.co/k9Ck1Bs/OIG2.png', 'francisco.lopez@email.org', '1234', 'usuario', NULL, 1),
(18, 16, 'PCastrox', 'Sevilla, España', 'Partidos de fútbol ', 'https://i.ibb.co/gdkKt55/OIG1.png', 'pedro.castro@mail.net', '1234', 'usuario', NULL, 1),
(19, 17, 'RaulRZ', 'Santiago, Chile', 'Eventos de comedia en vivo ', 'https://i.ibb.co/gdkKt55/OIG1.png', 'raul.ruiz@email.org', '1234', 'usuario', NULL, 1),
(20, 18, 'BOrtiz', 'Madrid, España', 'Eventos de comedia en vivo ', 'https://i.ibb.co/CBH2Nvx/OIG3.png', 'beatriz.ortega@mail.net', '1234', 'usuario', NULL, 1),
(21, 19, 'EMedinatronic', 'Madrid, España', 'Exhibiciones de autos clásicos ', 'https://i.ibb.co/fGLknC8/profile4.png', 'gabriela.rojas@example.org', '1234', 'usuario', NULL, 1),
(22, 20, 'SGuerrera', 'Cochabamba, Bolivia', 'Festivales de música electrónica', 'https://i.ibb.co/gdkKt55/OIG1.png', 'santaigo.guerrera@mail.net', '1234', 'usuario', NULL, 1),
(23, NULL, 'mfrias', NULL, NULL, 'https://i.ibb.co/gdkKt55/OIG1.png', 'prueba1@prueba.com', '1234', 'admin', NULL, 1),
(24, NULL, 'mfrias', NULL, NULL, 'https://i.ibb.co/fGLknC8/profile4.png', 'prueba2@prueba.com', '1234', 'usuario', NULL, 1),
(25, NULL, 'mfrias', NULL, NULL, 'https://i.ibb.co/CBH2Nvx/OIG3.png', 'prueba3@prueba.com', '1234', 'usuario', NULL, 1),
(26, NULL, 'goto', NULL, NULL, 'https://i.ibb.co/gdkKt55/OIG1.png', 'goto@goto.com', '1234', 'usuario', NULL, 1),
(27, NULL, 'holapapi', NULL, NULL, 'https://i.ibb.co/k9Ck1Bs/OIG2.png', 'patricio@mail.com', '1234', NULL, NULL, 1),
(32, NULL, 'jacobin', NULL, NULL, 'https://i.ibb.co/gdkKt55/OIG1.png', 'jacobo@mail.com', '$2y$13$mMmvOFa6rXOsK33D7P87Luo4BEakuRskuclyijkf/i1PqOSc31e2C', 'usuario', NULL, 1),
(33, NULL, 'miguel', NULL, NULL, 'https://i.ibb.co/gdkKt55/OIG1.png', 'miguel@mail.com', '$2y$13$eRBvrhoBlRdHvb9TiJiFXOvv/0TSQY1LmE4SpoycPs1WWr0XIwGM2', 'usuario', NULL, 1),
(34, NULL, 'jacobin', '', '', 'https://i.ibb.co/CBH2Nvx/OIG3.png', NULL, NULL, 'usuario', NULL, 1),
(35, NULL, '1', NULL, NULL, 'https://i.ibb.co/gdkKt55/OIG1.png', '1@mail.com', '$2y$13$kugOw.jo.lE1opgM3CCPLOudsEwUfsQee/ZVeeqDVg1ovm8bm9huK', 'usuario', NULL, 1),
(36, NULL, 'MaTTFR740', NULL, NULL, 'https://i.ibb.co/gdkKt55/OIG1.png', 'matt@mail.com', '$2y$13$nGQKZXmeP9v/oVVm4sQ8n.el4u4qJpERRw00MMZQH3NPo0vEDocUu', 'usuario', NULL, 1),
(37, NULL, 'MaTTFR740', 'Santander, Cantabria', 'Carreras de fórmula 1', 'https://i.ibb.co/gdkKt55/OIG1.png', 'matt1@mail.com', '$2y$13$nGQKZXmeP9v/oVVm4sQ8n.el4u4qJpERRw00MMZQH3NPo0vEDocUu', 'usuario', NULL, 1),
(39, NULL, 'ceinmark', 'Comillas, Cantabria', 'Festivales de música Rock', 'https://i.ibb.co/k9Ck1Bs/OIG2.png', 'ceinmark@mail.com', '$2y$13$B.Vd4pJcBBiPfbATcfEP8ubtJcfRPMHZfWelv6IgbSi4uVcVPqe2.', 'usuario', NULL, 1),
(40, NULL, 'Matt', 'España', 'Las carreras', 'https://i.ibb.co/gdkKt55/OIG1.png', 'mateofriasecheverria1@gmail.com', '$2y$13$97Z7lAR1MpuoL2WCaE.aLeSSvW2uT.v9e4yRhoowRsxUIPg/2RJ.G', 'usuario', NULL, 1),
(41, NULL, 'goto', NULL, NULL, NULL, 'gotoprueba@mail.com', '$2y$13$aVKeL530wDzfSrLujWgHBuIXEiQDEBjdUzKgCmanjMHHt8pqLsbnq', 'usuario', NULL, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `agentes`
--
ALTER TABLE `agentes`
  ADD PRIMARY KEY (`idAgente`);

--
-- Indices de la tabla `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`idCompra`),
  ADD KEY `idEvento` (`idEvento`),
  ADD KEY `idUsuario` (`idUsuario`);

--
-- Indices de la tabla `eventos`
--
ALTER TABLE `eventos`
  ADD PRIMARY KEY (`idEvento`),
  ADD KEY `fk_eventos_agentes` (`idAgente`),
  ADD KEY `fk_eventos_patrocinadores` (`idPatrocinador`);

--
-- Indices de la tabla `experiencias`
--
ALTER TABLE `experiencias`
  ADD PRIMARY KEY (`idExperiencia`),
  ADD KEY `fk_experiencias_eventos` (`idEvento`),
  ADD KEY `fk_experiencias_usuarios` (`idUsuario`);

--
-- Indices de la tabla `telefonosagentes`
--
ALTER TABLE `telefonosagentes`
  ADD PRIMARY KEY (`idTelefonoA`),
  ADD UNIQUE KEY `uk_idAgente_telefonoAgente` (`idAgente`,`telefonoAgente`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idUsuario`),
  ADD UNIQUE KEY `emails` (`emails`),
  ADD KEY `idAgente` (`idAgente`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `agentes`
--
ALTER TABLE `agentes`
  MODIFY `idAgente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `compras`
--
ALTER TABLE `compras`
  MODIFY `idCompra` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `eventos`
--
ALTER TABLE `eventos`
  MODIFY `idEvento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `experiencias`
--
ALTER TABLE `experiencias`
  MODIFY `idExperiencia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT de la tabla `telefonosagentes`
--
ALTER TABLE `telefonosagentes`
  MODIFY `idTelefonoA` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`idAgente`) REFERENCES `agentes` (`idAgente`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
