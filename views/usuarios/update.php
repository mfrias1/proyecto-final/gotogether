<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Usuarios $model */
/** @var yii\widgets\ActiveForm $form */
?>

<h1 class="text-center titulo-busqueda-eventos"><span>Crea un anuncio</span></h1>

<div class="form-custom">

    <p>Esto es lo que verán los demás usuarios de ti</p>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombreU')->textInput(['maxlength' => true, 'readonly' => !empty($model->nombreU)])->label('Nombre de usuario') ?>

    <?= $form->field($model, 'ubicacion')->textInput(['maxlength' => true])->label('Ubicación') ?>

    <?= $form->field($model, 'intereses')->textInput(['maxlength' => true])->label('Intereses') ?>

    <?= $form->field($model, 'imagenU')->hiddenInput()->label(false) ?>

    <div class="form-group">
        <label>Imagen de Perfil</label>
        <?= Html::button('Seleccionar Imagen', ['class' => 'btn btn-primary', 'data-toggle' => 'modal', 'data-target' => '#imageModal']) ?>
    </div>

    <div class="form-group text-right">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<!-- Modal -->
<div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="imageModalLabel">Seleccionar Imagen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <!-- Aquí se muestran las imágenes disponibles -->
                    <div class="col-md-3">
                        <?= Html::img("https://i.ibb.co/gdkKt55/OIG1.png", [
                            'class' => 'img-thumbnail image-option',
                            'data-image' => "https://i.ibb.co/gdkKt55/OIG1.png"
                        ]) ?>
                    </div>
                    <div class="col-md-3">
                        <?= Html::img("https://i.ibb.co/k9Ck1Bs/OIG2.png", [
                            'class' => 'img-thumbnail image-option',
                            'data-image' => "https://i.ibb.co/k9Ck1Bs/OIG2.png"
                        ]) ?>
                    </div>
                    <div class="col-md-3">
                        <?= Html::img("https://i.ibb.co/CBH2Nvx/OIG3.png", [
                            'class' => 'img-thumbnail image-option',
                            'data-image' => "https://i.ibb.co/CBH2Nvx/OIG3.png"
                        ]) ?>
                    </div>
                    <div class="col-md-3">
                        <?= Html::img("https://i.ibb.co/fGLknC8/profile4.png", [
                            'class' => 'img-thumbnail image-option',
                            'data-image' => "https://i.ibb.co/fGLknC8/profile4.png"
                        ]) ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<?php
$script = <<<JS
$('.image-option').on('click', function() {
    var imageUrl = $(this).data('image');
    $('#usuarios-imagenu').val(imageUrl);
    $('#imageModal').modal('hide');
});
JS;
$this->registerJs($script);
?>