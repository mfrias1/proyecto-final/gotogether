<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Usuarios $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="form-custom">

    <h2>Actualizar Perfil</h2>

    <?php $form = ActiveForm::begin([
        'action' => ['usuarios/update', 'id' => $model->idUsuario], // Define la acción del formulario
        'method' => 'post',
    ]); ?>

    <?= $form->field($model, 'nombreU')->textInput(['maxlength' => true, 'readonly' => !empty($model->nombreU)])->label('Nombre de usuario') ?>

    <?= $form->field($model, 'ubicacion')->textInput(['maxlength' => true])->label('Ubicación') ?>

    <?= $form->field($model, 'intereses')->textInput(['maxlength' => true])->label('Intereses') ?>

    <?= $form->field($model, 'imagenU')->hiddenInput()->label(false) ?>

    <div class="form-group">
        <label>Imagen de Perfil</label>
        <?= Html::button('Seleccionar Imagen', ['class' => 'btn btn-primary', 'data-toggle' => 'modal', 'data-target' => '#imageModal']) ?>
    </div>

    <?= Html::hiddenInput('redirect', 'index') ?>

    <div class="form-group text-right">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<!-- Modal -->
<div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="imageModalLabel">Seleccionar Imagen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <!-- Aquí debes agregar tus imágenes -->
                    <?php for ($i = 1; $i <= 4; $i++) : ?>
                        <div class="col-md-3">
                            <?= Html::img("@web/images/imagen$i.jpg", [
                                'class' => 'img-thumbnail image-option',
                                'data-image' => "imagen$i.jpg"
                            ]) ?>
                        </div>
                    <?php endfor; ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<?php
$script = <<<JS
$('.image-option').on('click', function() {
    var imageUrl = $(this).data('image');
    $('#usuarios-imagenu').val(imageUrl);
    $('#imageModal').modal('hide');
});
JS;
$this->registerJs($script);
?>
