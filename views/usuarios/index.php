<?php

use app\models\Usuarios;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
/** @var app\models\UsuariosSearch $searchModel */

?>
<div class="usuarios-index card-layout">

    <div class="text-center mb-4">
        <h1 class="text-center titulo-busqueda-eventos"><span>Encuentra tu compañero</span></h1>
    </div>

    <div class="filter-container mb-4">
        <?php $form = ActiveForm::begin([
            'method' => 'get',
            'action' => ['index'],
            'options' => ['class' => 'form-inline'],
        ]); ?>
        <div class="form-group">
            <?= $form->field($searchModel, 'intereses')->dropDownList(
                $searchModel->getInteresesList(),
                ['prompt' => 'Selecciona un interés', 'class' => 'form-control']
            )->label(false) ?>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Filtrar', ['class' => 'btn btn-primary']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

    <!-- Botón "Crea un anuncio" como primer elemento -->
    <div class="text-center mb-4">
        <?php if (!Yii::$app->user->isGuest) : ?>
            <?= Html::a('Crea un anuncio', ['usuarios/update', 'idUsuario' => Yii::$app->user->identity->idUsuario], ['class' => 'btn btn-primary-custom']) ?>
        <?php else : ?>
            <?= Html::a('Crea un anuncio', ['site/login'], ['class' => 'btn btn-primary-custom']) ?>
        <?php endif; ?>
    </div>

    <div class="card__container">
        <?php $counter = 0; ?>
        <?php foreach ($dataProvider->getModels() as $usuario) : ?>
            <?php if ($counter % 4 === 0) : ?>
                <div class="card-row">
                <?php endif; ?>

                <div class="card__article">
                    <?= Html::img($usuario->imagenU, [
                        'alt' => Html::encode($usuario->nombreU),
                        'class' => 'card__img img-fluid'
                    ]) ?>

                    <div class="card__data p-3">
                        <h2 class="card__title"><?= Html::encode($usuario->nombreU) ?></h2>
                        <p class="card__description"><strong>Intereses:</strong> <?= Html::encode($usuario->intereses) ?></p>
                        <p class="card__description"><strong>Email:</strong> <?= Html::encode($usuario->emails) ?></p>
                        <p class="card__description"><strong>Ubicación:</strong> <?= Html::encode($usuario->ubicacion) ?></p>
                    </div>
                </div>

                <?php $counter++; ?>

                <?php if ($counter % 4 === 0) : ?>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>

        <?php if ($counter % 4 !== 0) : // Cerrar fila abierta si no está completa ?>
    </div>
<?php endif; ?>
</div>

<div class="pagination-wrapper">
    <?= LinkPager::widget([
        'pagination' => $dataProvider->pagination,
    ]); ?>
</div>
</div>
