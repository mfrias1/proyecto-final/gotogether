<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap4\Modal;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\models\Usuarios $model */

?>
<div class="container mt-4">
    <div class="user-profile">
    <h1 class="text-center titulo-busqueda-eventos"><span>Panel de perfil</span></h1>

        <div class="grid-container">
            <!-- Sección 1: Información del Usuario y Anuncio Creado -->
            <div class="grid-item">
                <div class="form-custom">
                    <div class="user-info">
                        <h3>Información del Usuario</h3>
                        <div class="user-card">
                            <h4><strong>Nombre de Usuario:</strong><br> <?= Html::encode($model->nombreU) ?></h4>
                            <p><strong>Correo Electrónico:</strong><br> <?= Html::encode($model->emails) ?></p>
                        </div>
                    </div>

                    <?php if (!empty($model->intereses) && !empty($model->ubicacion) && !empty($model->imagenU)): ?>
                        <div class="user-announcement mt-4">
                            <h3>Anuncio Creado</h3>
                            <div class="card__article-no-animation">
                                <img src="<?= Html::encode($model->imagenU) ?>" alt="<?= Html::encode($model->nombreU) ?>" class="card__img-no-animation img-fluid mb-3">
                                <div class="card__data-no-animation">
                                    <p class="card__description"><strong>Intereses:</strong> <?= Html::encode($model->intereses) ?></p>
                                    <p class="card__description"><strong>Ubicación:</strong> <?= Html::encode($model->ubicacion) ?></p>
                                </div>
                            </div>
                            <p>
                                <?= Html::a('Cambiar Anuncio', ['usuarios/update', 'idUsuario' => $model->idUsuario], ['class' => 'btn btn-primary']) ?>
                            </p>
                        </div>
                    <?php else: ?>
                        <div class="user-announcement mt-4">
                            <div class="placeholder-image">
                                <img src="<?= Url::to('@web/images/Eventos/profile-image.jpg') ?>" alt="Placeholder" class="img-fluid mb-3 mask-effect">
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>

            <!-- Sección 2: Cambiar Contraseña y Eliminar Cuenta -->
            <div class="grid-item">
                <div class="form-custom">
                    <h3>Cambiar Contraseña</h3>
                    <?php $form = ActiveForm::begin(); ?>

                    <?= $form->field($model, 'currentPassword')->passwordInput() ?>
                    <?= $form->field($model, 'newPassword')->passwordInput() ?>
                    <?= $form->field($model, 'confirmNewPassword')->passwordInput() ?>

                    <div class="form-group">
                        <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>

                <div class="form-custom mt-4">
                    <div class="delete-account">
                        <h3>Eliminar Cuenta</h3>
                        <?= Html::button('Eliminar Cuenta', [
                            'class' => 'btn btn-danger-custom',
                            'data-toggle' => 'modal',
                            'data-target' => '#deleteModal'
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
Modal::begin([
    'id' => 'deleteModal',
    'title' => 'Eliminar Cuenta',
    'footer' => Html::a('Confirmar', ['delete', 'idUsuario' => $model->idUsuario], [
        'class' => 'btn btn-danger',
        'data' => [
            'method' => 'post',
        ],
    ]) . Html::button('Volver', [
        'class' => 'btn btn-secondary',
        'data-dismiss' => 'modal'
    ]),
]);

echo '<p>Estás a punto de borrar tu cuenta. ¿Estás seguro?</p>';

Modal::end();
?>
