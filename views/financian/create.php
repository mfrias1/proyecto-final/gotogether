<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Financian $model */

$this->title = 'Create Financian';
$this->params['breadcrumbs'][] = ['label' => 'Financians', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="financian-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
