<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Financian $model */

$this->title = 'Update Financian: ' . $model->idFinancian;
$this->params['breadcrumbs'][] = ['label' => 'Financians', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idFinancian, 'url' => ['view', 'idFinancian' => $model->idFinancian]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="financian-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
