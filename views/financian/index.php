<?php

use app\models\Financian;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Financians';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="financian-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Financian', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idFinancian',
            'idPatrocinador',
            'idEvento',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Financian $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idFinancian' => $model->idFinancian]);
                 }
            ],
        ],
    ]); ?>


</div>
