<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Financian $model */

$this->title = $model->idFinancian;
$this->params['breadcrumbs'][] = ['label' => 'Financians', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="financian-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'idFinancian' => $model->idFinancian], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'idFinancian' => $model->idFinancian], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idFinancian',
            'idPatrocinador',
            'idEvento',
        ],
    ]) ?>

</div>
