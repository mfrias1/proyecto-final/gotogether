<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Agentes $model */

$this->title = 'Create Agentes';
$this->params['breadcrumbs'][] = ['label' => 'Agentes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agentes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
