<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Agentes $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="agentes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombreA')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
