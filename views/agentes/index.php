<?php

use app\models\Agentes;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Agentes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agentes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Agentes', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idAgente',
            'nombreA',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Agentes $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idAgente' => $model->idAgente]);
                 }
            ],
        ],
    ]); ?>


</div>
