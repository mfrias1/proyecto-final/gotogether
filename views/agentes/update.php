<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Agentes $model */

$this->title = 'Update Agentes: ' . $model->idAgente;
$this->params['breadcrumbs'][] = ['label' => 'Agentes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idAgente, 'url' => ['view', 'idAgente' => $model->idAgente]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="agentes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
