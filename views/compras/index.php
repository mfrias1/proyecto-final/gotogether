<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="compras-index">

    <h1 class="text-center titulo-busqueda-eventos"><span>Carrito</span></h1>

    <?php if (!empty($cart)) : ?>
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-fixed">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Evento</th>
                        <th scope="col">Precio</th>
                        <th scope="col">Cantidad</th>
                        <th scope="col">Total</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($cart as $item) : ?>
                        <tr data-id="<?= $item['id'] ?>" data-price="<?= $item['price'] ?>">
                            <td><?= Html::encode($item['name']) ?></td>
                            <td><?= Html::encode(number_format($item['price'], 2)) ?>€</td>
                            <td>
                                <?= Html::input('number', 'quantity', $item['quantity'], [
                                    'min' => 1,
                                    'class' => 'form-control quantity-input',
                                    'style' => 'width: 80px; display: inline;'
                                ]) ?>
                            </td>
                            <td class="item-total"><?= Html::encode(number_format($item['price'] * $item['quantity'], 2)) ?>€</td>
                            <td>
                                <?= Html::a('Eliminar', ['compras/remove', 'id' => $item['id']], ['class' => 'btn btn-danger btn-sm']) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>

        <div class="text-center">
            <?= Html::a('Proceder al Pago', ['compras/checkout-stripe'], ['class' => 'btn btn-primary-custom']) ?>
        </div>
    <?php else : ?>
        <div class="empty-cart-container text-center">
            <p>El carrito está vacío</p>
            <?= Html::a('Explorar Eventos', ['eventos/index'], ['class' => 'btn btn-primary-custom']) ?>
        </div>
    <?php endif; ?>

</div>

<!-- Modal de confirmación -->
<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmModalLabel">Confirmación de Compra</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Estás a punto de comprar <span id="total-quantity"></span> entradas para el evento <span id="event-name"></span>. ¿Estás seguro?
            </div>
            <div class="modal-footer">
                <?= Html::a('Cancelar', ['eventos/index'], ['class' => 'btn btn-secondary']) ?>
                <?= Html::a('Confirmar', ['compras/confirm'], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>
</div>

<?php
$script = <<< JS
$(document).ready(function() {
    $('.quantity-input').on('change', function() {
        var quantity = $(this).val();
        var row = $(this).closest('tr');
        var id = row.data('id');
        var price = row.data('price');
        var newTotal = (quantity * price).toFixed(2);

        // Update the total price for the item
        row.find('.item-total').text(newTotal + '€');

        // Send the update to the server
        $.post('update-cart', { id: id, quantity: quantity }, function(data) {
            // Optionally handle response
        });
    });

    $('#confirmModal').on('show.bs.modal', function (event) {
        var totalQuantity = 0;
        var eventName = "";
        $('input.quantity-input').each(function() {
            totalQuantity += parseInt($(this).val());
            eventName = $(this).closest('tr').find('td:first').text();
        });
        $('#total-quantity').text(totalQuantity);
        $('#event-name').text(eventName);
    });
});
JS;
$this->registerJs($script);
?>
