<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Compras $model */

$this->title = $model->idCompra;
$this->params['breadcrumbs'][] = ['label' => 'Compras', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="compras-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'idCompra' => $model->idCompra], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'idCompra' => $model->idCompra], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idCompra',
            'idEvento',
            'idUsuario',
            'cantidad',
            'fechaCompra',
            'precioCompra',
            'estado',
            'total',
            'fechaModificacion',
        ],
    ]) ?>

</div>
