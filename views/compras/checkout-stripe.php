<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>

<h1 class="text-center titulo-busqueda-eventos"><span>Checkout</span></h1>

<?php if (Yii::$app->session->hasFlash('error')): ?>
    <div class="alert alert-danger">
        <?= Yii::$app->session->getFlash('error') ?>
    </div>
<?php endif; ?>

<!-- Tabla de entradas -->
<div class="table-responsive">
    <table class="table table-bordered table-striped table-small">
        <thead class="thead-dark">
            <tr>
                <th scope="col">Evento</th>
                <th scope="col">Precio</th>
                <th scope="col">Cantidad</th>
                <th scope="col">Total</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($cart as $item) : ?>
                <tr>
                    <td><?= Html::encode($item['name']) ?></td>
                    <td><?= Html::encode(number_format($item['price'], 2)) ?>€</td>
                    <td><?= Html::encode($item['quantity']) ?></td>
                    <td><?= Html::encode(number_format($item['price'] * $item['quantity'], 2)) ?>€</td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<div class="form-custom mt-2">
    <form action="<?= Url::to(['compras/charge']) ?>" method="post" id="payment-form">
        <?= Html::hiddenInput(Yii::$app->request->csrfParam, Yii::$app->request->csrfToken) ?>
        
        <h2>Formulario de Pago</h2>

        <div class="form-group">
            <label for="full-name">Nombre completo</label>
            <input type="text" id="full-name" name="full-name" class="form-control" required>
        </div>

        <div class="form-group">
            <label for="billing-address">Dirección de facturación</label>
            <input type="text" id="billing-address" name="billing-address" class="form-control" required>
        </div>

        <div class="form-group">
            <label for="card-element">Tarjeta de crédito o débito</label>
            <div id="card-element">
                <!-- Stripe's Element will be inserted here. -->
            </div>
            <div id="card-errors" class="invalid-feedback" role="alert"></div>
        </div>
        
        <p>Total a pagar: <?= number_format($totalAmount / 100, 2) ?>€</p>
        <input type="hidden" name="totalAmount" value="<?= $totalAmount ?>" />

        <div class="text-center mt-3">
            <?= Html::submitButton('Pagar', ['class' => 'btn btn-primary-custom']) ?>
        </div>
    </form>
</div>

<!-- Estilos CSS para Stripe Elements y tabla pequeña -->
<style>
    #card-element {
        border: 1px solid #ced4da;
        border-radius: .25rem;
        padding: .375rem .75rem;
        margin-bottom: .5rem;
        background-color: #333;
        color: var(--white);
    }
    .form-custom {
        max-width: 500px;
        margin: 0 auto;
        padding: 15px;
        background-color: var(--bg-color);
        border-radius: var(--border-radius);
        box-shadow: 0 0 15px rgba(0, 0, 0, 0.5);
        position: relative;
        overflow: hidden;
    }
    .form-custom::before, .form-custom::after {
        content: "";
        position: absolute;
        border-radius: 50%;
        filter: blur(20px);
    }
    .form-custom::before {
        width: 100px;
        height: 100px;
        background-color: var(--violet);
        top: -50px;
        left: -50px;
        z-index: -1;
    }
    .form-custom::after {
        width: 150px;
        height: 150px;
        background-color: #00aaff;
        bottom: -75px;
        right: -75px;
        z-index: -1;
    }
    .form-custom h2 {
        color: var(--white);
        font-size: 1.5rem;
        font-weight: bold;
        margin-bottom: 15px;
    }
    .form-custom label {
        display: block;
        color: #ccc;
        font-size: 0.875rem;
        margin-bottom: 5px;
    }
    .form-custom input,
    .form-custom textarea {
        width: 100%;
        padding: 10px;
        background-color: #333;
        border: 1px solid #555;
        border-radius: var(--border-radius);
        color: var(--white);
        margin-bottom: 10px;
    }
    .form-custom input:focus,
    .form-custom textarea:focus {
        outline: none;
        border-color: var(--violet);
    }
    .form-custom button {
        background: linear-gradient(90deg, #9f5afd 0%, #9f5afd 50%, #00aaff 100%);
        color: var(--white);
        padding: 10px 20px;
        border: none;
        border-radius: var(--border-radius);
        font-weight: bold;
        cursor: pointer;
        transition: opacity 0.3s;
    }
    .form-custom button:hover {
        opacity: 0.8;
    }
    .form-custom .invalid-feedback {
        color: #e3342f;
    }
    .table-small {
        max-width: 800px;
        margin: 0 auto;
        font-size: 0.875rem;
    }
</style>

<script src="https://js.stripe.com/v3/"></script>
<script>
    var stripe = Stripe('<?= Yii::$app->params['stripe'][Yii::$app->params['stripe.env']]['publishableKey'] ?>');
    var elements = stripe.elements();
    var card = elements.create('card', {
        style: {
            base: {
                color: '#fff',
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: 'antialiased',
                fontSize: '16px',
                '::placeholder': {
                    color: '#aab7c4'
                }
            },
            invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
            }
        }
    });
    card.mount('#card-element');

    card.addEventListener('change', function(event) {
        var displayError = document.getElementById('card-errors');
        if (event.error) {
            displayError.textContent = event.error.message;
        } else {
            displayError.textContent = '';
        }
    });

    var form = document.getElementById('payment-form');
    form.addEventListener('submit', function(event) {
        event.preventDefault();

        stripe.createToken(card).then(function(result) {
            if (result.error) {
                var errorElement = document.getElementById('card-errors');
                errorElement.textContent = result.error.message;
            } else {
                var hiddenInput = document.createElement('input');
                hiddenInput.setAttribute('type', 'hidden');
                hiddenInput.setAttribute('name', 'stripeToken');
                hiddenInput.setAttribute('value', result.token.id);
                form.appendChild(hiddenInput);

                form.submit();
            }
        });
    });
</script>
