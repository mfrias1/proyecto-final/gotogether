<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var array $cart */
/* @var string $purchaseTime */

?>
<div class="compra-confirmada">
<h1 class="text-center titulo-busqueda-eventos"><span>Compra confirmada</span></h1>
    <p class="text-center">Muchas gracias por tu compra. Aquí tienes tus entradas:</p>

    <div class="text-center">
        <?= Html::a('Ver Comprobante', ['compras/download-pdf', 'cart' => json_encode($cart), 'purchaseTime' => $purchaseTime], ['class' => 'btn btn-primary']) ?>
    </div>

    <h2 class="mt-4">Detalles de la Compra</h2>
    <p>Fecha y hora de compra: <?= Html::encode($purchaseTime) ?></p>

    <div class="table-responsive">
        <table class="table table-bordered table-striped table-small">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Evento</th>
                    <th scope="col">Precio</th>
                    <th scope="col">Cantidad</th>
                    <th scope="col">Total</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($cart as $item) : ?>
                    <tr>
                        <td><?= Html::encode($item['name']) ?></td>
                        <td><?= Html::encode(number_format($item['price'], 2)) ?>€</td>
                        <td><?= Html::encode($item['quantity']) ?></td>
                        <td><?= Html::encode(number_format($item['price'] * $item['quantity'], 2)) ?>€</td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<!-- Estilos CSS -->
<style>
    .compra-confirmada {
        max-width: 800px;
        margin: 0 auto;
        padding: 15px;
        background-color: var(--bg-color);
        border-radius: var(--border-radius);
        box-shadow: 0 0 15px rgba(0, 0, 0, 0.5);
        position: relative;
        overflow: hidden;
    }
    .compra-confirmada h1 {
        color: var(--white);
        font-size: 1.5rem;
        font-weight: bold;
        margin-bottom: 15px;
    }
    .compra-confirmada p {
        color: #ccc;
        font-size: 0.875rem;
        margin-bottom: 15px;
    }
    .compra-confirmada .btn-primary {
        background: linear-gradient(90deg, #9f5afd 0%, #9f5afd 50%, #00aaff 100%);
        color: var(--white);
        padding: 10px 20px;
        border: none;
        border-radius: var(--border-radius);
        font-weight: bold;
        cursor: pointer;
        transition: opacity 0.3s;
    }
    .compra-confirmada .btn-primary:hover {
        opacity: 0.8;
    }
    .table-small {
        max-width: 800px;
        margin: 0 auto;
        font-size: 0.875rem;
    }
    .table-responsive {
        margin-top: 20px;
    }
    .table-bordered {
        border: 1px solid #dee2e6;
    }
    .table-bordered th, .table-bordered td {
        border: 1px solid #dee2e6;
    }
    .thead-dark th {
        color: #fff;
        background-color: #343a40;
        border-color: #454d55;
    }
</style>
