<?php

use yii\helpers\Html;

/* @var array $cart */
/* @var string $purchaseTime */

// Define los estilos CSS en línea
$styles = "
    body { 
        background-color: #151515; 
        color: #fff; 
        font-family: Arial, sans-serif;
        margin: 20px;
    }
    .title {
        font-family: 'permanentmarker', cursive;
        font-weight: 400;
        font-style: normal;
        color: #ffffff;
        font-size: 24px;
        margin: 0 0 20px 0;
    }
    h1 { 
        color: #9f5afd; 
        margin-bottom: 10px;
    }
    p {
        margin: 10px 0;
    }
    .message {
        margin-bottom: 20px;
        font-size: 18px;
        color: #ffffff;
    }
    table {
        border-collapse: collapse;
        width: 100%;
        margin-top: 20px;
    }
    th, td {
        border: 1px solid #151515;
        padding: 10px;
        text-align: left;
        background-color: #333;
    }
    tbody{
        background-color:#151515;
    }
    th {
        background-color: #9f5afd;
        color: #fff;
    }
    tr:nth-child(even) {
        background-color: #333;
    }
    .logo-container {
        text-align: center;
        margin: 20px 0;
    }
    .logo {
        max-width: 100%;
        height: auto;
    }
";

$eventImagePath = Yii::getAlias('@webroot/images/Eventos/loollapaloozaarg.jpg');
$total = array_reduce($cart, function ($carry, $item) {
    return $carry + ($item['price'] * $item['quantity']);
}, 0);
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <title>Confirmación de Compra</title>
    <style>
        <?= $styles ?>
    </style>
</head>

<body>
    <div id="header">
        <div class="title">GoTogether</div>
    </div>
    <h1>Confirmación de Compra</h1>
    <p class="message">¡Muchas gracias por la compra! Esperamos que lo disfrutes.</p>
    <div class="logo-container">
        <img src="<?= $eventImagePath ?>" alt="Evento" class="logo">
    </div>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Evento</th>
                <th>Precio</th>
                <th>Cantidad</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($cart as $item) : ?>
                <tr>
                    <td><?= Html::encode($item['name']) ?></td>
                    <td><?= Html::encode(number_format($item['price'], 2)) ?>€</td>
                    <td><?= Html::encode($item['quantity']) ?></td>
                    <td></td>
                </tr>
            <?php endforeach; ?>
            <tr>
                <td colspan="3" style="text-align: right;"><strong>Total</strong></td>
                <td><?= Html::encode(number_format($total, 2)) ?>€</td>
            </tr>
        </tbody>
    </table>
</body>

</html>