<?php

use yii\helpers\Html;

$this->title = 'Pago Cancelado';
?>

<h1><?= Html::encode($this->title) ?></h1>

<p>El pago ha sido cancelado.</p>