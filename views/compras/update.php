<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Compras $model */

$this->title = 'Update Compras: ' . $model->idCompra;
$this->params['breadcrumbs'][] = ['label' => 'Compras', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idCompra, 'url' => ['view', 'idCompra' => $model->idCompra]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="compras-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
