<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;

$this->title = 'Olvidé mi contraseña';
?>
<div class="site-request-password-reset">
    <div class="form-custom">
        <h2><?= Html::encode($this->title) ?></h2>


        <?php $form = ActiveForm::begin([
            'id' => 'request-password-reset-form',
            'layout' => 'horizontal',
            'fieldConfig' => [
                'template' => "{label}{input}\n{error}",
                'labelOptions' => ['class' => 'col-form-label'],
                'inputOptions' => ['class' => 'form-control'],
                'errorOptions' => ['class' => 'invalid-feedback'],
            ],
        ]); ?>

        <?= $form->field($model, 'emails')->textInput(['autofocus' => false])->label('Introduce tu correo electrónico') ?>

        <div class="form-group">
            <div>
                <p>Si tu email está registrado te mandaremos un correo para que puedas reestablecer tu contraseña.</p>
            </div>
        </div>

        <div class="form-group">
            <div>
                <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>