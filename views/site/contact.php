<?php

/** @var yii\web\View $this */
/** @var yii\bootstrap4\ActiveForm $form */
/** @var app\models\ContactForm $model */

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\captcha\Captcha;

?>
<div class="site-contact">
    <h1 class="text-center titulo-busqueda-eventos"><span>Tu opinión nos importa</span></h1>

    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

        <div class="alert alert-success text-center">
            Gracias por contactar con nosotros, nos pondremos en contacto lo antes posible.
        </div>

        <p class="text-center">
            Note that if you turn on the Yii debugger, you should be able
            to view the mail message on the mail panel of the debugger.
            <?php if (Yii::$app->mailer->useFileTransport): ?>
                Because the application is in development mode, the email is not sent but saved as
                a file under <code><?= Yii::getAlias(Yii::$app->mailer->fileTransportPath) ?></code>.
                Please configure the <code>useFileTransport</code> property of the <code>mail</code>
                application component to be false to enable email sending.
            <?php endif; ?>
        </p>

    <?php else: ?>

        <p class="text-center">
            Si tienes cualquier sugerencia no dudes en hacérnoslo saber, llena esto y tu
            feedback nos llegará.
        </p>

        <div class="row justify-content-center">
            <div class="col-lg-7 form-custom contact-form-custom">

                <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                    <?= $form->field($model, 'name')->textInput(['autofocus' => true, 'class' => 'form-control'])->label('Nombre', ['class' => 'control-label']) ?>

                    <?= $form->field($model, 'email')->textInput(['class' => 'form-control'])->label('Correo Electrónico', ['class' => 'control-label']) ?>

                    <?= $form->field($model, 'subject')->textInput(['class' => 'form-control'])->label('Asunto', ['class' => 'control-label']) ?>

                    <?= $form->field($model, 'body')->textarea(['rows' => 6, 'class' => 'form-control'])->label('Cuerpo del Mensaje', ['class' => 'control-label']) ?>

                    <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                        'template' => '<div class="row"><div class="col-lg-12 text-center">{image}</div><div class="col-lg-12 mt-2">{input}</div></div>',
                    ])->label('Código de Verificación', ['class' => 'control-label']) ?>

                    <div class="form-group">
                        <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary-custom', 'name' => 'contact-button']) ?>
                    </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>

    <?php endif; ?>
</div>
