<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

?>
<div class="site-login">
    <div class="form-custom">
        <h1 class="text-center titulo-busqueda-eventos"><span>¡Bienvenido!</span></h1>

        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'layout' => 'horizontal',
            'fieldConfig' => [
                'template' => "{label}{input}\n{error}",
                'labelOptions' => ['class' => 'col-form-label'],
                'inputOptions' => ['class' => 'form-control'],
                'errorOptions' => ['class' => 'invalid-feedback'],
            ],
        ]); ?>

        <?= $form->field($model, 'emails')->textInput(['autofocus' => true]) ?>

        <?= $form->field($model, 'password')->passwordInput() ?>

        <div class="form-group d-flex justify-content-between align-items-center">
            <div>
                <?= Html::a('¿Olvidaste tu contraseña?', ['site/request-password-reset'], ['class' => 'btn btn-link']) ?>
            </div>
            <div>
                <?= $form->field($model, 'rememberMe', [
                    'template' => "<div class=\"custom-control custom-checkbox\">{input} {label}</div>\n<div>{error}</div>",
                ])->checkbox() ?>
            </div>
        </div>

        <div class="form-group text-center p-4">
            <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
