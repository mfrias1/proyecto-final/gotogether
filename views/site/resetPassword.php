<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;


?>
<div class="site-reset-password">
    <div class="form-custom">
        <h2>Reestablecer contraseña</h2>

        <p>Introduce tu nueva contraseña</p>

        <?php $form = ActiveForm::begin([
            'id' => 'reset-password-form',
            'layout' => 'horizontal',
            'fieldConfig' => [
                'template' => "{label}{input}\n{error}",
                'labelOptions' => ['class' => 'col-form-label'],
                'inputOptions' => ['class' => 'form-control'],
                'errorOptions' => ['class' => 'invalid-feedback'],
            ],
        ]); ?>

        <?= $form->field($model, 'password')->passwordInput(['autofocus' => true])->label('Nueva contraseña') ?>

        <?= $form->field($model, 'password_repeat')->passwordInput()->label('Repite tu nueva contraseña') ?>

        <div class="form-group">
            <div>
                <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>