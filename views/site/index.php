<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Inicio';

// Título
$imageUrl = Url::to('@web/images/eventos/index-image.jpg');
?>

<div class="hero-1 fade-in-section section-margin">
    <div class="outer">
        <div class="inner">
            <div class="hero-wrapper">
                <div class="text">
                    <div class="title">
                        <h1 class="text-anime">
                            <div class="line-wrapper">
                                <div class="text-lines"><span>GoToGether </span></div>
                            </div>
                        </h1>
                    </div>
                    <p class="subheading text-anime">
                        <div class="line-wrapper">
                            <div class="text-lines text-gray-h">EMPIEZA A DESCUBRIR NUEVAS EXPERIENCIAS</div>
                        </div>
                    </p>
                </div>
                <div class="image image-anime revealed">
                    <?php
                    echo Html::img($imageUrl, [
                        'alt' => 'hero',
                        'class' => 'animated-image'
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom-area fade-in">
        <div class="area-container">
            <div class="row">
                <div class="col-xl-6 col-md-2 col-6 arrow-container">
                </div>
                <div class="col-xl-6 col-md-10 col-6">
                    <div class="social-trigger">
                        <p>follow us</p>
                        <i class="fas fa-arrow-right"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Primer bloque -->
<div class="bloque1 fade-in-section section-margin">
    <div class="container mt-4">
        <div class="row">
            <div class="col-md-6">
                <?= Html::img('@web/images/people.png', ['alt' => 'gente', 'class' => 'img-fluid']) ?>
            </div>
            <div class="col-md-6">
                <h2 class="text-uppercase">ENCUENTRA GENTE</h2>
                <p>Descubre nuevas experiencias y conecta con personas que comparten tus intereses.
                    Encuentra compañía para eventos emocionantes y crea memorias juntos. La aplicación te permite explorar,
                    socializar y disfrutar de eventos únicos con una comunidad vibrante. ¡Encuentra gente y vive momentos inolvidables!</p>
            </div>
        </div>
    </div>
</div>

<!-- Segundo bloque -->
<div class="bloque2 sin-margen fade-in-section section-margin">
    <div class="container mt-4">
        <div class="row">
            <div class="col-md-6 order-md-2">
                <?= Html::img('@web/images/notep.jpg', ['alt' => 'ASISTE', 'class' => 'img-fluid']) ?>
            </div>
            <div class="col-md-6 order-md-1">
                <h2 class="text-uppercase">NO TE PIERDAS NADA</h2>
                <p> No te pierdas ni un detalle de los eventos que amas. Con nuestra aplicación,
                    estarás al tanto de las últimas noticias sobre conciertos, partidos y más.
                    Obtén alertas personalizadas, descubre nuevas experiencias y asegúrate de
                    no perderte ninguna oportunidad de diversión. Conéctate con lo que amas, ¡no te pierdas nada!</p>
            </div>
        </div>
    </div>
</div>

<!-- Tercer bloque con imágenes desplazables -->
<div class="row row-eventos fade-in-section section-margin">
    <div class="col-12">
        <h2 class="mb-4">Eventos más cercanos</h2>
        <div class="slider-container">
            <div class="slider-track">
                <?php if (!empty($eventos)) : ?>
                    <?php foreach ($eventos as $evento) : ?>
                        <div class="slide">
                            <div class="card">
                                <div class="card-background" style="background-image: url('<?= Html::encode($evento->imagenE) ?>');"></div>
                                <div class="description">
                                    <p class="date"><?= Html::encode($evento->fecha) ?></p>
                                    <h4 class="n-evento"><?= Html::encode($evento->nombrEvento) ?></h4>
                                    <p class="lugar"><?= Html::encode($evento->lugar) ?></p>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php else : ?>
                    <div class="col-12">
                        <p>No hay eventos disponibles.</p>
                    </div>
                <?php endif; ?>
            </div>
            <button class="prev" aria-label="Previous">&#10094;</button>
            <button class="next" aria-label="Next">&#10095;</button>
        </div>
    </div>
</div>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        // Fade-in 
        const observerOptions = {
            root: null,
            rootMargin: '0px',
            threshold: 0.1
        };

        const fadeInElements = document.querySelectorAll('.fade-in-section');

        const observer = new IntersectionObserver((entries, observer) => {
            entries.forEach(entry => {
                if (entry.isIntersecting) {
                    entry.target.classList.add('visible');
                    observer.unobserve(entry.target);
                }
            });
        }, observerOptions);

        fadeInElements.forEach(element => {
            observer.observe(element);
        });

        // Slider 
        const track = document.querySelector('.slider-track');
        const slides = Array.from(track.children);
        const nextButton = document.querySelector('.next');
        const prevButton = document.querySelector('.prev');
        const slideWidth = slides[0].getBoundingClientRect().width;

        let index = 0;

        function moveSlider() {
            track.style.transition = 'transform 0.5s ease';
            track.style.transform = `translateX(-${index * slideWidth}px)`;
        }

        function updateIndex(newIndex) {
            index = newIndex;
            moveSlider();
        }

        nextButton.addEventListener('click', () => {
            index++;
            if (index >= slides.length) {
                index = 0;
            }
            updateIndex(index);
        });

        prevButton.addEventListener('click', () => {
            index--;
            if (index < 0) {
                index = slides.length - 1;
            }
            updateIndex(index);
        });

        setInterval(() => {
            index++;
            if (index >= slides.length) {
                index = 0;
            }
            updateIndex(index);
        }, 3000); // Cambiar cada 3 segundos
    });
</script>
