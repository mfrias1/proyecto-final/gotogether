<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Eventos $model */
/** @var app\models\Agentes $agente */
/** @var app\models\Telefonosagentes[] $telefonosAgente */

$this->title = $model->nombrEvento;
\yii\web\YiiAsset::register($this);
?>
<div class="eventos-view">

    <div class="container p-0">

    <div class="single-news-hero">
            <div class="image"></div>
            <div class="row row-1 detalle-evento" style="position: relative; height:100%;">
                <div class="col" style="position: absolute; bottom: 0;">
                    <div class="fecha"><?= Html::encode(date('d-m-Y', strtotime($model->fecha))) ?></div>
                    <h1><?= Html::encode($this->title) ?></h1>
                    <div class="categoria"><?= Html::encode($model->categoria) ?></div>
                </div>
            </div>
        </div>

        <div class="row mx-auto mt-4">
            <div class="col">
                <p class="descripcion"><?= Html::encode($model->descripcion) ?></p>
                <p class="precio"><strong>Precio:</strong> <?= Html::encode(number_format($model->precio, 2)) ?>€</p>

                <div class="agente-info">
                    <?php if ($agente) : ?>
                        <p><strong>Agente del evento:</strong> <?= Html::encode($agente->nombreA) ?></p>
                        <p><strong>Teléfonos:</strong></p>
                        <ul>
                            <?php foreach ($telefonosAgente as $telefono) : ?>
                                <li><?= Html::encode($telefono->telefonoAgente) ?></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php else : ?>
                        <p>No hay información del agente disponible.</p>
                    <?php endif; ?>
                </div>

                <!--Mandar info al carrito -->
                <?php if (Yii::$app->user->isGuest) : ?>
                    <p><?= Html::a('Agregar al carrito', ['site/login'], ['class' => 'btn btn-primary']) ?></p>
                <?php else : ?>
                    <?php $form = ActiveForm::begin(['action' => ['compras/add']]); ?>
                    <?= Html::hiddenInput('id', $model->idEvento) ?>
                    <?= Html::hiddenInput('cantidad', 1) ?>
                    <div class="form-group">
                        <?= Html::submitButton('Añadir entrada al carrito', ['class' => 'btn btn-primary']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                <?php endif; ?>

            </div>
        </div>
    </div>

</div>