<?php

use yii\helpers\Html;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\models\Eventos[] $resultados */

?>

<?php if (!empty($resultados)) : ?>
    <div class="row tarjetas">
        <?php foreach ($resultados as $evento) : ?>
            <div class="col-12 col-md-4 mb-4">
                <a href="<?= Url::to(['eventos/view', 'idEvento' => $evento->idEvento]) ?>" class="card-link">
                    <div class="card contenedor">
                        <img src="<?= Html::encode($evento->imagenE) ?>" alt="<?= Html::encode($evento->nombrEvento) ?>" class="card-img-top imagen-evento">
                        <div class="card-body">
                            <p class="card-text fecha"><?= Html::encode($evento->fecha) ?></p>
                            <h4 class="card-title titulo"><?= Html::encode($evento->nombrEvento) ?></h4>
                        </div>
                    </div>
                </a>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
