<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Eventos';

// Función para convertir la fecha
function formatearFecha($fecha)
{
    setlocale(LC_TIME, 'es_ES.UTF-8'); // Establecer la configuración regional en español
    $timestamp = strtotime($fecha);
    return strftime('%e de %B, %Y', $timestamp);
}
?>

<div class="container">
    <div class="eventos-home">
        <div class="row">
            <div class="col">
                <h1 class="text-center titulo-busqueda-eventos"><span>Buscar Eventos</span></h1>
                <form id="buscador-form" method="GET">
                    <input type="text" name="query" id="query" placeholder="Buscar eventos" class="form-control buscador-pequeno">
                </form>
                <div id="resultados"></div>
                <p id="no-resultados" style="display:none;">No se encontraron eventos.</p>
            </div>
        </div>
       
        <div class="row">
            <div class="burguer">
                <div class="container">
                    <div class="row tarjetas" id="resultados-eventos">
                        <?php if ($dataProvider->getCount() > 0) : ?>
                            <?php foreach ($dataProvider->getModels() as $evento) : ?>
                                <div class="col-12 col-md-4 mb-4">
                                    <a href="<?= Url::to(['eventos/view', 'idEvento' => $evento->idEvento]) ?>" class="card-link">
                                        <div class="card contenedor">
                                            <img src="<?= Html::encode($evento->imagenE) ?>" alt="<?= Html::encode($evento->nombrEvento) ?>" class="card-img-top imagen-evento" onerror="this.onerror=null;this.src='<?= Url::to('@web/images/default.png') ?>';">
                                            <div class="card-body">
                                                <p class="card-text fecha"><?= Html::encode(formatearFecha($evento->fecha)) ?></p>
                                                <h4 class="card-title titulo"><?= Html::encode($evento->nombrEvento) ?></h4>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            <?php endforeach; ?>
                        <?php else : ?>
                            <p>No hay eventos disponibles.</p>
                        <?php endif; ?>
                    </div>
                    <div class="pagination-wrapper">
                        <?= LinkPager::widget([
                            'pagination' => $dataProvider->pagination,
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#query').on('keyup', function() {
            var query = $(this).val().trim();
            if (query.length === 0) {
                $('#resultados-eventos').show(); // Muestra los eventos iniciales si no hay búsqueda
                $('#resultados').empty();
                $('#no-resultados').hide();
                return;
            } else {
                $('#resultados-eventos').hide(); // Oculta los eventos iniciales
            }
            $.ajax({
                url: '<?= Url::to(['eventos/buscar']) ?>',
                type: 'GET',
                data: {
                    query: query
                },
                success: function(response) {
                    $('#resultados').html(response);
                    if (!$.trim(response)) {
                        $('#no-resultados').show();
                    } else {
                        $('#no-resultados').hide();
                    }
                }
            });
        });
    });
</script>
