<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\RegistroUsuario $model */

$this->title = 'Update Registro Usuario: ' . $model->idUsuario;
$this->params['breadcrumbs'][] = ['label' => 'Registro Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idUsuario, 'url' => ['view', 'idUsuario' => $model->idUsuario]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="registro-usuario-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
