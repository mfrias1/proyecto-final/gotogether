<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap4\Alert;

/* @var $this yii\web\View */
/* @var $model app\models\RegistroUsuario */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-custom">
    <h4 class="text-center titulo-busqueda-eventos"><span>Regístrate</span>  </h4>
    <p>¿Listo para nuevas experiencias?</p>
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombreU')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'emails')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true, 'value' => '']) ?>

    <?php if ($model->hasErrors()) : ?>
        <?= Alert::widget([
            'options' => ['class' => 'alert-danger'],
            'body' => Html::errorSummary($model),
        ]); ?>
    <?php endif; ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
