<?php

use app\models\Asisten;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Asistens';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asisten-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Asisten', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idAsisten',
            'idUsuario',
            'idEvento',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Asisten $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idAsisten' => $model->idAsisten]);
                 }
            ],
        ],
    ]); ?>


</div>
