<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Asisten $model */

$this->title = 'Update Asisten: ' . $model->idAsisten;
$this->params['breadcrumbs'][] = ['label' => 'Asistens', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idAsisten, 'url' => ['view', 'idAsisten' => $model->idAsisten]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="asisten-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
