<?php

use app\models\Telefonosagentes;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Telefonosagentes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="telefonosagentes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Telefonosagentes', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idTelefonoA',
            'idAgente',
            'telefonoAgente',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Telefonosagentes $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idTelefonoA' => $model->idTelefonoA]);
                 }
            ],
        ],
    ]); ?>


</div>
