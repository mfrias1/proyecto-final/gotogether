<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Telefonosagentes $model */

$this->title = 'Create Telefonosagentes';
$this->params['breadcrumbs'][] = ['label' => 'Telefonosagentes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="telefonosagentes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
