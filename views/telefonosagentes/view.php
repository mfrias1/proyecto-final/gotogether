<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Telefonosagentes $model */

$this->title = $model->idTelefonoA;
$this->params['breadcrumbs'][] = ['label' => 'Telefonosagentes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="telefonosagentes-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'idTelefonoA' => $model->idTelefonoA], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'idTelefonoA' => $model->idTelefonoA], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idTelefonoA',
            'idAgente',
            'telefonoAgente',
        ],
    ]) ?>

</div>
