<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Telefonosagentes $model */

$this->title = 'Update Telefonosagentes: ' . $model->idTelefonoA;
$this->params['breadcrumbs'][] = ['label' => 'Telefonosagentes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idTelefonoA, 'url' => ['view', 'idTelefonoA' => $model->idTelefonoA]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="telefonosagentes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
