<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Telefonosagentes $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="telefonosagentes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idAgente')->textInput() ?>

    <?= $form->field($model, 'telefonoAgente')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
