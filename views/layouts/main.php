<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\helpers\Url;

AppAsset::register($this);

$this->registerCsrfMetaTags();
$this->registerMetaTag(['charset' => Yii::$app->charset], 'charset');
$this->registerMetaTag(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1, shrink-to-fit=no']);
$this->registerMetaTag(['name' => 'description', 'content' => $this->params['meta_description'] ?? '']);
$this->registerMetaTag(['name' => 'keywords', 'content' => $this->params['meta_keywords'] ?? '']);
$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/x-icon', 'href' => Yii::getAlias('@web/images/favicon.ico')]); 
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">

<head>
    <title>
        Gotogether
    </title>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    <?php $this->head() ?>
</head>

<body class="d-flex flex-column h-100">
    <?php $this->beginBody() ?>

    <header id="header">
        <?php
        NavBar::begin([
            'options' => ['class' => 'navbar-expand-md navbar-global h-100']
        ]);

        // Contenedor del logo
        echo Html::a(
            '<div class="text-container">' .
                '<div class="navbar-title">Gotogether</div>' .
                '</div>',
            Yii::$app->homeUrl,
            ['class' => 'navbar-brand']
        );

        echo Nav::widget([
            'options' => ['class' => 'navbar-nav'],
            'items' => [
                [
                    'label' => 'Inicio',
                    'url' => ['/site/index'],
                    'options' => ['class' => 'animate__animated animate__fadeIn']
                ],
                [
                    'label' => 'Eventos',
                    'url' => ['/eventos/index'],
                    'options' => ['class' => 'animate__animated animate__fadeIn']
                ],
                [
                    'label' => 'Compañeros',
                    'url' => ['/usuarios/index'],
                    'options' => ['class' => 'animate__animated animate__fadeIn']
                ],
                [
                    'label' => 'Experiencias',
                    'url' => ['/experiencias/index'],
                    'options' => ['class' => 'animate__animated animate__fadeIn']
                ],
                [
                    'label' => 'Contacto',
                    'url' => ['/site/contact'],
                    'options' => ['class' => 'animate__animated animate__fadeIn']
                ],
            ],
        ]);

        echo '<div class="container-login-registro">';

        if (Yii::$app->user->isGuest) {
            // Mostrar botones de login y registrar si el usuario no está logueado
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav'],
                'items' => [
                    [
                        'label' => '<i class="fas fa-sign-in-alt"></i> Login',
                        'url' => ['/site/login'],
                        'encode' => false,
                        'options' => ['class' => 'btn-login animate__animated animate__fadeIn']
                    ],
                    [
                        'label' => 'Registrar',
                        'url' => ['/registro-usuario/create'],
                        'options' => ['class' => 'rounded-pill animate__animated animate__fadeIn btn-registrar']
                    ],
                ],
            ]);
        } else {
            // Obtener el usuario logueado
            $usuario = Yii::$app->user->identity;
            $nombreUsuario = $usuario->nombreU;
            $imagenUsuario = $usuario->imagenU ? Html::img($usuario->imagenU, ['class' => 'navbar-profile-img']) : '';

            // Mostrar el nombre de usuario y el botón de logout si el usuario está logueado
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav'],
                'items' => [
                    [
                        'label' => $imagenUsuario . Html::encode($nombreUsuario),
                        'url' => ['/usuarios/profile'],  // Asegúrate de que esta URL apunta a la acción de perfil del usuario
                        'encode' => false,
                        'options' => ['class' => 'nav-link animate__animated animate__fadeIn d-flex align-items-center']
                    ],
                    [
                        'label' => 'Carrito' . (isset(Yii::$app->session['cartCount']) && Yii::$app->session['cartCount'] > 0 ? ' (' . Yii::$app->session['cartCount'] . ')' : ''),
                        'url' => ['/compras/index'],
                        'encode' => false,
                        'options' => ['class' => 'animate__animated animate__fadeIn']
                    ],
                    [
                        'label' => 'Logout',
                        'url' => ['/site/logout'],
                        'linkOptions' => ['data-method' => 'post'],
                        'options' => ['class' => 'nav-link animate__animated animate__fadeIn']
                    ],
                ],
            ]);
        }

        echo '</div>'; // Cierra el contenedor div para los elementos Login y Registrar

        NavBar::end();
        ?>
    </header>

    <main id="main" class="main" role="main">
        <div class="container container-principal">
            <?php if (!empty($this->params['breadcrumbs'])) : ?>
                <?= Breadcrumbs::widget([
                    'links' => $this->params['breadcrumbs'],
                    'options' => ['class' => 'bread-general']
                ]) ?>
            <?php endif ?>
            <?= Alert::widget() ?>

            <?php
            // Mostrar el nombre de usuario registrado si está en la sesión
            if (Yii::$app->session->has('registeredUserName')) {
                echo '<div class="alert alert-success">Usuario registrado: ' . Yii::$app->session->get('registeredUserName') . '</div>';
                // Eliminar el nombre de usuario de la sesión después de mostrarlo
                Yii::$app->session->remove('registeredUserName');
            }
            ?>

            <?= $content ?>
        </div>
    </main>

    <footer id="footer" class="footer">
        <div class="container mt-0">
            <div class="row row-1 mb-4">
                <div class="col d-flex justify-content-center">
                    <span class="title">mfriase@ceinmark.net</span>
                </div>
            </div>

            <div class="row row-2 contain-redes">
                <div class="col d-flex justify-content-center">
                    <?= Html::a('Gitlab', 'https://www.facebook.com', ['class' => 'social-link', 'target' => '_blank']) ?>
                    <?= Html::a('Github', 'https://www.twitter.com', ['class' => 'social-link mx-3', 'target' => '_blank']) ?>
                    <?= Html::a('LinkedIn', 'https://www.instagram.com', ['class' => 'social-link', 'target' => '_blank']) ?>
                </div>
            </div>

            <div class="row row-3 contain-redes">
                <div class="col d-flex justify-content-center">
                    <span class="gotogether">gotogether</span>
                </div>
            </div>

            <div class="row row-4 contain-redes">
                <div class="col d-flex justify-content-center">
                    <span class="copyright">© 2024 Gotogether / Diseño por Mateo</span>
                </div>
            </div>

            <div class="footer-right-image"></div>

        </div>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>
