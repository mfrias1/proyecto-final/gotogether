<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Telefonospatrocinadores $model */

$this->title = 'Update Telefonospatrocinadores: ' . $model->idTelefonoP;
$this->params['breadcrumbs'][] = ['label' => 'Telefonospatrocinadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idTelefonoP, 'url' => ['view', 'idTelefonoP' => $model->idTelefonoP]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="telefonospatrocinadores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
