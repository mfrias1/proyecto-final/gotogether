<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Telefonospatrocinadores $model */

$this->title = $model->idTelefonoP;
$this->params['breadcrumbs'][] = ['label' => 'Telefonospatrocinadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="telefonospatrocinadores-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'idTelefonoP' => $model->idTelefonoP], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'idTelefonoP' => $model->idTelefonoP], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idTelefonoP',
            'idPatrocinador',
            'telefonoPatrocinador',
        ],
    ]) ?>

</div>
