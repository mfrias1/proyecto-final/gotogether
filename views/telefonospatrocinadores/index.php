<?php

use app\models\Telefonospatrocinadores;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Telefonospatrocinadores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="telefonospatrocinadores-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Telefonospatrocinadores', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idTelefonoP',
            'idPatrocinador',
            'telefonoPatrocinador',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Telefonospatrocinadores $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idTelefonoP' => $model->idTelefonoP]);
                 }
            ],
        ],
    ]); ?>


</div>
