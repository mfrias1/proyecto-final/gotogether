<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Telefonospatrocinadores $model */

$this->title = 'Create Telefonospatrocinadores';
$this->params['breadcrumbs'][] = ['label' => 'Telefonospatrocinadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="telefonospatrocinadores-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
