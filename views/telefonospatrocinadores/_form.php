<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Telefonospatrocinadores $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="telefonospatrocinadores-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idPatrocinador')->textInput() ?>

    <?= $form->field($model, 'telefonoPatrocinador')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
