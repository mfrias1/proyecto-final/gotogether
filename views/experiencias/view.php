<?php

use yii\helpers\Html;
use yii\web\View;

/** @var yii\web\View $this */
/** @var app\models\Experiencias $model */

\yii\web\YiiAsset::register($this);

// Fetching related models
$evento = \app\models\Eventos::findOne($model->idEvento);
$usuario = \app\models\User::findOne($model->idUsuario);

?>
<div class="experiencias-view container">

    <div class="header">
    <h1 class="text-center titulo-busqueda-eventos"><span>Tu experiencia</span></h1>
</div>

    <div class="event-row">
                <div class="event-details">
                    <div class="event-date-location">
                        <div class="event-location"><?= Html::encode($model->evento->nombrEvento) ?></div>
                        <span class="nameuser"><i class="fa-regular fa-user"></i><?= Html::encode($model->usuario->nombreU) ?></span>
                    </div>
                    <div class="event-time">
                        <span class="comment"><i class="fa-regular fa-comment"></i> <?= Html::encode($model->comentario) ?></span>
                        <span><i class="fa-solid fa-star"></i> <?= Html::encode($model->calificacion) ?></span>
                        <span class="event-date"><i class="fa-regular fa-calendar"></i> <?= Html::encode(date('d-m-Y', strtotime($model->fecha))) ?></span>
                    </div>
                </div>
                <?php if (Yii::$app->user->id == $model->idUsuario) : ?>
                    <div class="event-actions">
                        <?= Html::a('Editar', ['update', 'idExperiencia' => $model->idExperiencia], ['class' => 'btn-update']) ?>
                        <?= Html::a('Borrar', ['delete', 'idExperiencia' => $model->idExperiencia], [
                            'class' => 'btn-delete',
                            'data' => [
                                'confirm' => '¿Estás seguro de que quieres borrar?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </div>
                <?php endif; ?>
            </div>

</div>

