<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ExperienciasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

setlocale(LC_TIME, 'es_ES.UTF-8');

?>
<div class="experiencias-index container">

    <h1 class="text-center titulo-busqueda-eventos"><span>Experiencias</span></h1>

    <div class="filter-container mb-4">
        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
            'options' => ['class' => 'form-inline w-100'], 
        ]); ?>
        <div class="row w-100 justify-content-center">
            <div class="col-md-4 mb-2">
                <?= $form->field($searchModel, 'idEvento')->dropDownList(
                    \yii\helpers\ArrayHelper::map(\app\models\Eventos::find()->all(), 'idEvento', 'nombrEvento'),
                    ['prompt' => 'Seleccione un evento', 'class' => 'form-control w-100']
                )->label(false) ?>
            </div>
            <div class="col-md-4 mb-2">
                <?= $form->field($searchModel, 'fecha')->input('date', ['class' => 'form-control w-100'])->label(false) ?>
            </div>
            <div class="col-md-2 mb-2">
                <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary w-100']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

    <div class="text-center mb-4">
        <?= Html::a('Añadir Experiencia', ['create'], ['class' => 'btn btn-primary-custom']) ?>
    </div>

    <div class="events-container">
        <?php foreach ($dataProvider->models as $model) : ?>
            <div class="event-row">
                <div class="event-details">
                    <div class="event-date-location">
                        <div class="event-location"><?= Html::encode($model->evento->nombrEvento) ?></div>
                        <span class="nameuser"><i class="fa-regular fa-user"></i><?= Html::encode($model->usuario->nombreU) ?></span>
                    </div>
                    <div class="event-time">
                        <span class="comment"><i class="fa-regular fa-comment"></i> <?= Html::encode($model->comentario) ?></span>
                        <span><i class="fa-solid fa-star"></i> <?= Html::encode($model->calificacion) ?></span>
                        <span class="event-date"><i class="fa-regular fa-calendar"></i> <?= Html::encode(date('d-m-Y', strtotime($model->fecha))) ?></span>
                    </div>
                </div>
                <?php if (Yii::$app->user->id == $model->idUsuario) : ?>
                    <div class="event-actions">
                        <?= Html::a('Editar', ['update', 'idExperiencia' => $model->idExperiencia], ['class' => 'btn-update']) ?>
                        <?= Html::a('Borrar', ['delete', 'idExperiencia' => $model->idExperiencia], [
                            'class' => 'btn-delete',
                            'data' => [
                                'confirm' => '¿Estás seguro de que quieres borrar?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </div>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
    </div>
</div>
