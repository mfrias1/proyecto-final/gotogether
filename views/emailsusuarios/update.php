<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Emailsusuarios $model */

$this->title = 'Update Emailsusuarios: ' . $model->idEmail;
$this->params['breadcrumbs'][] = ['label' => 'Emailsusuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idEmail, 'url' => ['view', 'idEmail' => $model->idEmail]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="emailsusuarios-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
