<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Emailsusuarios $model */

$this->title = $model->idEmail;
$this->params['breadcrumbs'][] = ['label' => 'Emailsusuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="emailsusuarios-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'idEmail' => $model->idEmail], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'idEmail' => $model->idEmail], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idEmail:email',
            'idUsuario',
            'emails:email',
        ],
    ]) ?>

</div>
