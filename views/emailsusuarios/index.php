<?php

use app\models\Emailsusuarios;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Emailsusuarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emailsusuarios-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Emailsusuarios', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idEmail:email',
            'idUsuario',
            'emails:email',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Emailsusuarios $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idEmail' => $model->idEmail]);
                 }
            ],
        ],
    ]); ?>


</div>
