<?php

return [
    'adminEmail' => 'supergotogether@hotmail.com',
    'senderEmail' => 'supergotogether@hotmail.com',
    'senderName' => 'Gotogether',
    'user.passwordResetTokenExpire' => 3600, // 1 hora


    // Define el entorno ('test' o 'live')
    'stripe.env' => 'test',

    // Claves de API de Stripe
    'stripe' => [
             // MODO PRUEBA
        'test' => [
            'secretKey' => 'sk_test_4eC39HqLyjWDarjtT1zdp7dc',
            'publishableKey' => 'pk_test_TYooMQauvdEDq54NiTphI7jx',
        ],
            // MODO PRODUCCION
        'live' => [
            'secretKey' => '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',
            'publishableKey' => 'pk_live_51PMmyHCPOuqr4OryAu1bRUPmKP8CkGa8t4899Q20Yy5jViLycYOsFWbj0EsV5tYF5hIkvaAQwnXvoEYNqhzddsBv00Dq1Id2W5',
        ],
    ],
];
