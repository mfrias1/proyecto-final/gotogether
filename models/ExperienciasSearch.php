<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Experiencias;

/**
 * ExperienciasSearch represents the model behind the search form of `app\models\Experiencias`.
 */
class ExperienciasSearch extends Experiencias
{
    public function rules()
    {
        return [
            [['idExperiencia', 'idEvento', 'idUsuario'], 'integer'],
            [['calificacion'], 'number'],
            [['comentario', 'fecha'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Experiencias::find();

        // join with related tables
        $query->joinWith(['evento', 'usuario']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // add conditions that should always apply here

        // filter conditions
        $query->andFilterWhere([
            'experiencias.idExperiencia' => $this->idExperiencia,
            'experiencias.idEvento' => $this->idEvento,
            'experiencias.idUsuario' => $this->idUsuario,
            'experiencias.calificacion' => $this->calificacion,
            'DATE(experiencias.fecha)' => $this->fecha, // ensure the table alias is used
        ]);

        $query->andFilterWhere(['like', 'experiencias.comentario', $this->comentario]);

        return $dataProvider;
    }
}
