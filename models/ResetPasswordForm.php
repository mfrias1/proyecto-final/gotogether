<?php

namespace app\models;

use yii\base\Model;
use yii\base\InvalidArgumentException;
use app\models\User;

class ResetPasswordForm extends Model
{
    public $password;
    public $password_repeat;

    /**
     * @var \app\models\User
     */
    private $_user;

    /**
     * Creates a form model given a token.
     *
     * @param string $token
     * @param array $config name-value pairs that will be used to initialize the object properties
     * @throws InvalidArgumentException if token is empty or not valid
     */
    public function __construct($token, $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidArgumentException('Password reset token cannot be blank.');
        }
        $this->_user = User::findByPasswordResetToken($token);
        if (!$this->_user) {
            throw new InvalidArgumentException('Wrong password reset token.');
        }
        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['password', 'password_repeat'], 'required', 'message' => 'Este campo no puede estar vacío'],
            ['password', 'string', 'min' => 6, 'message' => 'La contraseña debe tener al menos 6 caracteres'],
            ['password', 'match', 'pattern' => '/[A-Z]/', 'message' => 'La contraseña debe contener al menos una letra mayúscula'],
            ['password', 'match', 'pattern' => '/[a-z]/', 'message' => 'La contraseña debe contener al menos una letra minúscula'],
            ['password', 'match', 'pattern' => '/[\W_]/', 'message' => 'La contraseña debe contener al menos un carácter especial'],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => "Las contraseñas no coinciden"],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'password' => 'Nueva contraseña',
            'password_repeat' => 'Repite tu nueva contraseña',
        ];
    }

    /**
     * Resets password.
     *
     * @return bool if password was reset.
     */
    public function resetPassword()
    {
        $user = $this->_user;
        $user->setPassword($this->password);
        $user->removePasswordResetToken();

        return $user->save(false);
    }
}
