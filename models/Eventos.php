<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "eventos".
 *
 * @property int $idEvento
 * @property string|null $nombrEvento
 * @property string|null $fecha
 * @property string|null $lugar
 * @property string|null $categoria
 * @property string|null $descripcion
 * @property string|null $imagenE
 * @property int|null $idPatrocinador
 * @property int|null $idAgente
 *
 * @property Asisten[] $asistens
 * @property Experiencias[] $experiencias
 * @property Financian[] $financians
 * @property Agentes $idAgente0
 * @property Patrocinadores $idPatrocinador0
 * @property Patrocinadores[] $idPatrocinadors
 * @property Usuarios[] $idUsuarios
 */
class Eventos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'eventos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
            [['idPatrocinador', 'idAgente'], 'integer'],
            [['precio'], 'number'],
            [['nombrEvento', 'lugar', 'categoria'], 'string', 'max' => 100],
            [['descripcion'], 'string', 'max' => 250],
            [['imagenE'], 'string', 'max' => 500],
            [['idAgente'], 'exist', 'skipOnError' => true, 'targetClass' => Agentes::class, 'targetAttribute' => ['idAgente' => 'idAgente']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idEvento' => 'Id Evento',
            'nombrEvento' => 'Nombr Evento',
            'fecha' => 'Fecha',
            'lugar' => 'Lugar',
            'categoria' => 'Categoria',
            'descripcion' => 'Descripcion',
            'imagenE' => 'Imagen E',
            'precio' => 'Precio',
            'idAgente' => 'Id Agente',
        ];
    }


    /**
     * Gets query for [[Experiencias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getExperiencias()
    {
        return $this->hasMany(Experiencias::class, ['idEvento' => 'idEvento']);
    }

    /**
     * Gets query for [[IdAgente0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdAgente0()
    {
        return $this->hasOne(Agentes::class, ['idAgente' => 'idAgente']);
    }

    /**
     * Gets query for [[IdUsuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsuarios()
    {
        return $this->hasMany(Usuarios::class, ['idUsuario' => 'idUsuario'])->viaTable('asisten', ['idEvento' => 'idEvento']);
    }
}
