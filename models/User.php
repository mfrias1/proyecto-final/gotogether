<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use Yii;

class User extends ActiveRecord implements IdentityInterface
{
    public $authKey;
    public $accessToken;

    // Definir constantes de roles
    const ROL_ADMIN = 'administrador';
    const ROL_USUARIO = 'usuario';

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * @return string the name of the table associated with this ActiveRecord class.
     */
    public static function tableName()
    {
        return 'usuarios';
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($idUsuario)
    {
        return static::findOne($idUsuario);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->idUsuario;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Encuentra un usuario por su dirección de correo electrónico
     * @param string $emails
     * @return User|null
     */
    public static function findByEmail($emails)
    {
        return static::findOne(['emails' => $emails, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Encuentra un usuario por su token de restablecimiento de contraseña
     * @param string $token token de restablecimiento de contraseña
     * @return User|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Determina si el token de restablecimiento de contraseña es válido
     * @param string $token token de restablecimiento de contraseña
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * Genera un token de restablecimiento de contraseña
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Elimina el token de restablecimiento de contraseña
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Establece la contraseña
     * @param string $password contraseña
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Valida la contraseña
     * @param string $password contraseña
     * @return bool si la contraseña es válida para el usuario actual
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * Verifica si el usuario es administrador
     * @return bool
     */
    public function esAdmin()
    {
        return $this->rol === self::ROL_ADMIN;
    }
}
