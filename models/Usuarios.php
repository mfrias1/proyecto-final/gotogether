<?php
namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "usuarios".
 *
 * @property int $idUsuario
 * @property string|null $nombreU
 * @property string|null $ubicacion
 * @property string|null $intereses
 * @property string|null $imagenU
 * @property string $emails
 * @property string $password
 */
class Usuarios extends ActiveRecord implements IdentityInterface
{
    public $currentPassword;
    public $newPassword;
    public $confirmNewPassword;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombreU', 'emails', 'password'], 'required'],
            [['ubicacion', 'intereses'], 'string', 'max' => 250],
            [['imagenU'], 'string', 'max' => 500],
            [['emails'], 'email'],
            [['currentPassword', 'newPassword', 'confirmNewPassword'], 'required', 'on' => 'changePassword'],
            [['newPassword', 'confirmNewPassword'], 'string', 'min' => 6],
            ['confirmNewPassword', 'compare', 'compareAttribute' => 'newPassword', 'message' => 'Las contraseñas no coinciden.'],
            ['currentPassword', 'validateCurrentPassword'],
        ];
    }

    /**
     * Validate current password
     */
    public function validateCurrentPassword($attribute, $params)
    {
        if (!$this->validatePassword($this->currentPassword)) {
            $this->addError($attribute, 'Contraseña actual incorrecta.');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idUsuario' => 'ID Usuario',
            'nombreU' => 'Nombre de Usuario',
            'ubicacion' => 'Ubicación',
            'intereses' => 'Intereses',
            'imagenU' => 'Imagen de Perfil',
            'emails' => 'Correo Electrónico',
            'password' => 'Contraseña',
            'currentPassword' => 'Contraseña Actual',
            'newPassword' => 'Nueva Contraseña',
            'confirmNewPassword' => 'Confirmar Nueva Contraseña',
        ];
    }

    /**
     * Sets new password
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Validates password
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    // Implement the required methods for IdentityInterface
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        // Not implemented
        return null;
    }

    public function getId()
    {
        return $this->idUsuario;
    }

    public function getAuthKey()
    {
        // Not implemented
        return null;
    }

    public function validateAuthKey($authKey)
    {
        // Not implemented
        return null;
    }
}
