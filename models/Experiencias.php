<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "experiencias".
 *
 * @property int $idExperiencia
 * @property int|null $idEvento
 * @property int|null $idUsuario
 * @property float|null $calificacion
 * @property string|null $comentario
 * @property string|null $fecha
 *
 * @property Eventos $evento
 * @property Usuarios $usuario
 */
class Experiencias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'experiencias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idEvento', 'idUsuario'], 'integer'],
            [['calificacion'], 'number'],
            [['fecha'], 'safe'],
            [['comentario'], 'string', 'max' => 255],
            [['idEvento'], 'required'], 
            [['idEvento'], 'exist', 'skipOnError' => true, 'targetClass' => Eventos::class, 'targetAttribute' => ['idEvento' => 'idEvento']],
            [['idUsuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::class, 'targetAttribute' => ['idUsuario' => 'idUsuario']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idExperiencia' => 'Id Experiencia',
            'idEvento' => 'Evento',
            'idUsuario' => 'Usuario',
            'calificacion' => 'Calificación',
            'comentario' => 'Comentario',
            'fecha' => 'Fecha',
        ];
    }

    /**
     * Gets query for [[Evento]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEvento()
    {
        return $this->hasOne(Eventos::class, ['idEvento' => 'idEvento']);
    }

    /**
     * Gets query for [[Usuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuarios::class, ['idUsuario' => 'idUsuario']);
    }

    /**
     * Gets experiencias with event and user details.
     *
     * @return array
     */
    public static function getExperienciasWithDetails()
    {
        return self::find()
            ->select([
                'experiencias.idExperiencia',
                'eventos.nombrEvento AS nombreEvento',
                'usuarios.nombreU AS nombreUsuario',
                'experiencias.calificacion',
                'experiencias.comentario',
                'experiencias.fecha'
            ])
            ->joinWith(['evento', 'usuario'])
            ->asArray()
            ->all();
    }
}
