<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "telefonosagentes".
 *
 * @property int $idTelefonoA
 * @property int|null $idAgente
 * @property string|null $telefonoAgente
 *
 * @property Agentes $idAgente0
 */
class Telefonosagentes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'telefonosagentes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idAgente'], 'integer'],
            [['telefonoAgente'], 'string', 'max' => 25],
            [['idAgente', 'telefonoAgente'], 'unique', 'targetAttribute' => ['idAgente', 'telefonoAgente']],
            [['idAgente'], 'exist', 'skipOnError' => true, 'targetClass' => Agentes::class, 'targetAttribute' => ['idAgente' => 'idAgente']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idTelefonoA' => 'Id Telefono A',
            'idAgente' => 'Id Agente',
            'telefonoAgente' => 'Telefono Agente',
        ];
    }

    /**
     * Gets query for [[IdAgente0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdAgente0()
    {
        return $this->hasOne(Agentes::class, ['idAgente' => 'idAgente']);
    }
}
