<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

class UsuariosSearch extends Usuarios
{
    public function rules()
    {
        return [
            [['intereses'], 'safe'],
        ];
    }

    public function search($params)
    {
        $query = Usuarios::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder' => [
                    'idUsuario' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'intereses', $this->intereses]);

        return $dataProvider;
    }

    public function getInteresesList()
    {
        $intereses = Usuarios::find()
            ->select('intereses')
            ->distinct()
            ->orderBy('intereses')
            ->asArray()
            ->all();

        return \yii\helpers\ArrayHelper::map($intereses, 'intereses', 'intereses');
    }
}
