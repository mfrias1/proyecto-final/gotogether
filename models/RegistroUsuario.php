<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * RegistroUsuario model
 */
class RegistroUsuario extends ActiveRecord implements IdentityInterface
{
    public $authKey;
    public $accessToken;

    // Definir constantes de roles
    const ROL_ADMIN = 'administrador';
    const ROL_USUARIO = 'usuario';

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * @return string the name of the table associated with this ActiveRecord class.
     */
    public static function tableName()
    {
        return 'usuarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['emails', 'password', 'nombreU'], 'required', 'message' => 'El campo {attribute} no puede estar en blanco.'],
            ['emails', 'email', 'message' => 'El formato del correo electrónico no es válido.'],
            ['emails', 'unique', 'targetClass' => self::class, 'message' => 'Este correo electrónico ya está registrado.'],
            [['emails', 'password', 'nombreU'], 'string', 'max' => 255],
            ['password', 'string', 'min' => 6, 'tooShort' => 'La contraseña debe tener al menos 6 caracteres.'],
            ['password', 'match', 'pattern' => '/[A-Z]/', 'message' => 'La contraseña debe contener al menos una letra mayúscula.'],
            ['password', 'match', 'pattern' => '/[a-z]/', 'message' => 'La contraseña debe contener al menos una letra minúscula.'],
            ['password', 'match', 'pattern' => '/\d/', 'message' => 'La contraseña debe contener al menos un número.'],
            ['password', 'match', 'pattern' => '/[\W_]/', 'message' => 'La contraseña debe contener al menos un carácter especial.'],
            ['nombreU', 'match', 'pattern' => '/^[a-zA-Z0-9]+$/', 'message' => 'El nombre de usuario solo puede contener letras y números.'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idUsuario' => 'ID',
            'emails' => 'Correo Electrónico',
            'password' => 'Contraseña',
            'nombreU' => 'Nombre de Usuario',
        ];
    }

    /**
     * Establece la contraseña
     * @param string $password contraseña
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Valida la contraseña
     * @param string $password contraseña
     * @return bool si la contraseña es válida para el usuario actual
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * Encuentra un usuario por su dirección de correo electrónico
     * @param string $emails
     * @return RegistroUsuario|null
     */
    public static function findByEmail($emails)
    {
        return static::findOne(['emails' => $emails]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->idUsuario;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }
}
