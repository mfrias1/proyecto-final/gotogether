<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\base\InvalidArgumentException;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $emails;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['emails', 'trim'],
            ['emails', 'required'],
            ['emails', 'email'],
            ['emails', 'exist',
                'targetClass' => '\app\models\User',
                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => 'There is no user with this email address.'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne([
            'emails' => $this->emails,
            'status' => User::STATUS_ACTIVE,
        ]);

        if (!$user) {
            return false;
        }
        
        if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            if (!$user->save()) {
                return false;
            }
        }

        Yii::debug('Buscando las vistas de email en: ' . Yii::getAlias('@app/mail'));

        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
            ->setTo($this->emails)
            ->setSubject('Password reset for ' . Yii::$app->name)
            ->send();
    }

}
