<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "compras".
 *
 * @property int $idCompra
 * @property int|null $idEvento
 * @property int|null $idUsuario
 * @property int|null $cantidad
 * @property string|null $fechaCompra
 * @property float|null $precioCompra
 * @property string|null $estado
 * @property float|null $total
 * @property string $fechaModificacion
 */
class Compras extends \yii\db\ActiveRecord
{
    // Define la constante ESTADO_PAGADO
    const ESTADO_PAGADO = 'pagado';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'compras';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idEvento', 'idUsuario', 'cantidad'], 'integer'],
            [['fechaCompra', 'fechaModificacion'], 'safe'],
            [['precioCompra', 'total'], 'number'],
            [['estado'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idCompra' => 'Id Compra',
            'idEvento' => 'Id Evento',
            'idUsuario' => 'Id Usuario',
            'cantidad' => 'Cantidad',
            'fechaCompra' => 'Fecha Compra',
            'precioCompra' => 'Precio Compra',
            'estado' => 'Estado',
            'total' => 'Total',
            'fechaModificacion' => 'Fecha Modificacion',
        ];
    }
}
