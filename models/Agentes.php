<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "agentes".
 *
 * @property int $idAgente
 * @property string|null $nombreA
 *
 * @property Eventos[] $eventos
 * @property Telefonosagentes[] $telefonosagentes
 * @property Usuarios[] $usuarios
 */
class Agentes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'agentes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombreA'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idAgente' => 'Id Agente',
            'nombreA' => 'Nombre A',
        ];
    }

    /**
     * Gets query for [[Eventos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEventos()
    {
        return $this->hasMany(Eventos::class, ['idAgente' => 'idAgente']);
    }

    /**
     * Gets query for [[Telefonosagentes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTelefonosagentes()
    {
        return $this->hasMany(Telefonosagentes::class, ['idAgente' => 'idAgente']);
    }

    /**
     * Gets query for [[Usuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasMany(Usuarios::class, ['idAgente' => 'idAgente']);
    }
}
